package com.mygdx.mario.screens.editorWidgets;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.mario.Tile;
import com.mygdx.mario.Tile.Type;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.entities.enemies.Goomba;
import com.mygdx.mario.entities.enemies.Koopa;
import com.mygdx.mario.entities.items.Coin;
import com.mygdx.mario.screens.EditorScreen;
import com.mygdx.mario.screens.EditorScreen.State;

public class ThingPicker {
	private Stage stage;
	private Table container;
	
	private Tile.Type selectedTileType;
	private Entity.Type selectedEntityType;
	
	final SelectBox<Things> thingSelector;
	final SelectBox<String> itemSelector;
	
	public enum Things {
		TILES,
		ENTITIES;
	}
	
	public ThingPicker(final EditorScreen screen) {
		Skin skin = screen.getSkin();
		stage = new Stage();
		screen.addInputProcessor(stage);
		
		container = new Table();
		stage.addActor(container);
		container.setFillParent(true);
		container.right().top();
		
		final Table innerTileContainer = new Table();
		innerTileContainer.pad(10, 5, 20, 5);
		final Table innerEntityContainer = new Table();
		innerEntityContainer.pad(10, 5, 10, 5);
		
		final ScrollPane tileScroll = new ScrollPane(innerTileContainer, skin);
		final ScrollPane entityScroll = new ScrollPane(innerEntityContainer, skin);
		
		thingSelector = new SelectBox<Things>(skin);
		thingSelector.setItems(Things.values());
		
		thingSelector.addListener(new ChangeListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				switch(((SelectBox<Things>)actor).getSelected()) {
					case TILES:
						container.removeActor(entityScroll);
						container.row();
						container.add(tileScroll).prefWidth(200);
						container.row();
						container.add(itemSelector).minWidth(200);
						break;
					case ENTITIES:
						container.removeActor(tileScroll);
						container.removeActor(itemSelector);
						container.row();
						container.add(entityScroll).prefWidth(200);
				}
			}
		});
		
		String[] items = new String[]{"None", "Coin", "5 Coins", "10 Coins", "up_mushroom", "magic_mushroom"};
		itemSelector = new SelectBox<String>(skin);
		itemSelector.setItems(items);
		
		final Pixmap pm1 = new Pixmap(1, 1, Format.RGB565);
		pm1.setColor(Color.LIGHT_GRAY);
		pm1.fill();
		
		for (final Entity.Type entType: Entity.Type.values()) {
			TextureRegion entTexture;
			switch(entType) {
				case MARIO:
					entTexture = Mario.getBaseTexture();
					break;
				case GOOMBA:
					entTexture = Goomba.getBaseTexture();
					break;
				case KOOPA:
					entTexture = Koopa.getBaseTexture();
					break;
				case COIN:
					entTexture = Coin.getBaseTexture();
					break;
				default:
					continue;
			}
			
			final Table table = new Table();
			
			Image entImage = new Image(entTexture);
			table.add(entImage).prefSize(20).pad(0, 0, 0, 5);
			table.add(new Label(entType.name(), skin)).expandX().fillX();
			
			table.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (screen.getState() == State.PAUSED) {
						return;
					}
					
					selectedEntityType = entType;
					
					for (Actor tab: innerEntityContainer.getChildren()) {
						((Table) tab).setBackground((Drawable) null);
					}
					
					table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
				}
			});
			
			innerEntityContainer.row();
			innerEntityContainer.add(table).expand().fill();
			
			if (entType == Entity.Type.MARIO) {
				table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
				selectedEntityType = Entity.Type.MARIO;
			}
		}
	    
		for (final Tile.Type tileType: Tile.Type.values()) {
			if (tileType.getTexture() == null) {
				continue;
			}
			
			final Table table = new Table();
			
			table.add(new Image(tileType.getTexture())).pad(0, 0, 0, 5);
			table.add(new Label(tileType.name(), skin)).expandX().fillX();
			
			table.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (screen.getState() == State.PAUSED || tileType == selectedTileType) {
						return;
					}
					
					selectedTileType = tileType;
					
					for (Actor tab: innerTileContainer.getChildren()) {
						((Table) tab).setBackground((Drawable) null);
					}
					
					table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
					
					itemSelector.setSelected("None");
					if (selectedTileType == Type.BRICK || selectedTileType == Type.BOX) {
						itemSelector.setVisible(true);
					} else {
						itemSelector.setVisible(false);
					}
				}
			});
			
			innerTileContainer.row();
			innerTileContainer.add(table).expand().fill();
			
			if (tileType == Type.BRICK) {
				table.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));
				selectedTileType = Type.BRICK;
			}
		}
		
		container.add(thingSelector).minWidth(200);
		
		container.row();
		container.add(tileScroll).prefWidth(200);
		
		container.row();
		container.add(itemSelector).minWidth(200);
	}
	
	
	public void render() {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	public void dispose() {
		stage.dispose();
	}
	
	
	public void disableInputs() {
		for(Actor actor: stage.getActors()) {
			actor.setTouchable(Touchable.disabled);
		}
	}
	
	public void enableInputs() {
		for(Actor actor: stage.getActors()) {
			actor.setTouchable(Touchable.enabled);
		}
	}
	
	
	public Things getSelectedThing() {
		return thingSelector.getSelected();
	}
	
	public Tile.Type getSelectedTileType() {
		return selectedTileType;
	}
	
	public Entity.Type getSelectedEntityType() {
		return selectedEntityType;
	}
	
	public String getSelectedItem() {
		if (!itemSelector.isVisible()) {
			return null;
		}
		
		return itemSelector.getSelected();
	}
}
