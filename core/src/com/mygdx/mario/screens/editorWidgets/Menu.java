package com.mygdx.mario.screens.editorWidgets;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.mario.LevelData;
import com.mygdx.mario.screens.EditorScreen;

public class Menu {
	private Stage stage;
	private EditorScreen screen;
	private Skin skin;
	
	private Table mainMenu;
	private Table saveMenu;
	private Table loadMenu;
	private Table optionsMenu;
	
	private TextField fileNameInput;
	private SelectBox<String> fileSelector;
	
	public Menu(final EditorScreen screen) {
		this.screen = screen;
		this.skin = screen.getSkin();
		stage = new Stage();
		screen.addInputProcessor(stage);
		
		mainMenu = createMainMenu();
		saveMenu = createSaveMenu();
		loadMenu = createLoadMenu();
		optionsMenu = createOptionsMenu();
	}
	
	private Table createMainMenu() {
		Table container = new Table();
		container.setFillParent(true);
		container.setVisible(false);
		
		final Table innerContainer = new Table(skin);
		innerContainer.pad(20).defaults().expandX().space(10);
		
		
		final TextButton playButton = new TextButton("PLAY", skin, "default");
		
		playButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				screen.playLevel();
			}
		});
		
		innerContainer.row();
		innerContainer.add(playButton).size(100f, 30f).pad(20, 0, 0, 0);
		
		
		final TextButton saveButton = new TextButton("SAVE", skin, "default");
		
		saveButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				mainMenu.setVisible(false);
				saveMenu.setVisible(true);
			}
		});
		
		innerContainer.row();
		innerContainer.add(saveButton).size(100f, 30f);
		
		
		final TextButton loadButton = new TextButton("LOAD", skin, "default");
		
		loadButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				mainMenu.setVisible(false);
				loadMenu.setVisible(true);
			}
		});
		
		innerContainer.row();
		innerContainer.add(loadButton).size(100f, 30f);
		
		
		final TextButton optionsButton = new TextButton("OPTIONS", skin, "default");
		
		optionsButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				mainMenu.setVisible(false);
				optionsMenu.setVisible(true);
			}
		});
		
		innerContainer.row();
		innerContainer.add(optionsButton).size(100f, 30f);
		
		
		final TextButton exitButton = new TextButton("EXIT", skin, "default");
		
		exitButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				screen.closeEditor();
			}
		});
		
		innerContainer.row();
		innerContainer.add(exitButton).size(100f, 30f);
		
		
		innerContainer.setBackground("default-window");
		container.add(innerContainer);
		stage.addActor(container);
		
		return container;
	}
	
	private Table createSaveMenu() {
		Table container = new Table();
		container.setFillParent(true);
		container.setVisible(false);
		
		final Table innerContainer = new Table(skin);
		innerContainer.pad(20).defaults().expandX().space(10);
		

		Label titleLabel = new Label("SAVE LEVEL", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		
		innerContainer.row().colspan(2).center().pad(10, 0, 0, 0);
		innerContainer.add(titleLabel);
		
		
		final TextField finalFileNameInput = new TextField(screen.getFileName(), skin);
		fileNameInput = finalFileNameInput;
		
		innerContainer.row().colspan(2).center().pad(10, 0, 0, 0);
		innerContainer.add(finalFileNameInput).size(200f, 30f);
		
		
		final TextButton cancelButton = new TextButton("CANCEL", skin, "default");
		
		cancelButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				openMainMenu();
			}
		});
		
		innerContainer.row().pad(10, 0, 0, 0);
		innerContainer.add(cancelButton).size(80f, 30f).left();
		
		
		final TextButton saveButton = new TextButton("SAVE", skin, "default");
		
		saveButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				String fileName = finalFileNameInput.getText();
				if (fileName != null && !fileName.isEmpty()) {
					screen.saveLevel(fileName);
				}
			}
		});
		
		innerContainer.add(saveButton).size(80f, 30f).right();
		
		
		innerContainer.setBackground("default-window");
		container.add(innerContainer);
		stage.addActor(container);
		
		return container;
	}
	
	private Table createLoadMenu() {
		Table container = new Table();
		container.setFillParent(true);
		container.setVisible(false);
		
		final Table innerContainer = new Table(skin);
		innerContainer.pad(20).defaults().expandX().space(10);
		
		
		Label titleLabel = new Label("LOAD LEVEL", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		
		innerContainer.row().colspan(2).center().pad(10, 0, 0, 0);
		innerContainer.add(titleLabel);
		
		
		String[] files = LevelData.listLevelFiles();
		final SelectBox<String> finalFileSelector = new SelectBox<String>(skin);
		finalFileSelector.setItems(files);
		fileSelector = finalFileSelector;
		
		innerContainer.row().colspan(2).center().pad(10, 0, 0, 0);
		innerContainer.add(finalFileSelector).size(150f, 30f);
		
		
		final TextButton cancelButton = new TextButton("CANCEL", skin, "default");
		
		cancelButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				openMainMenu();
			}
		});
		
		innerContainer.row().pad(10, 0, 0, 0);
		innerContainer.add(cancelButton).size(80f, 30f).left();
		
		
		final TextButton loadButton = new TextButton("LOAD", skin, "default");
		
		loadButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				String selectedFile = finalFileSelector.getSelected();
				if (selectedFile != null) {
					screen.loadLevel(selectedFile);
				}
			}
		});
		
		innerContainer.add(loadButton).size(80f, 30f).right();
		
		
		innerContainer.setBackground("default-window");
		container.add(innerContainer);
		stage.addActor(container);
		
		return container;
	}
	
	private Table createOptionsMenu() {
		Table container = new Table();
		container.setFillParent(true);
		container.setVisible(false);
		
		final Table innerContainer = new Table(skin);
		innerContainer.pad(20).defaults().expandX().space(10);

		
		Label titleLabel = new Label("OPTIONS", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		
		innerContainer.row().colspan(2).center().pad(10, 0, 0, 0);
		innerContainer.add(titleLabel);
		
		
		Label levelSizeLabel = new Label("Level width: ", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
	
		innerContainer.row().pad(10, 0, 0, 0);
		innerContainer.add(levelSizeLabel);
		
		
		final TextField levelSizeInput = new TextField("110", skin);
		levelSizeInput.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
		levelSizeInput.setMaxLength(4);
		
		innerContainer.add(levelSizeInput).size(50f, 30f).left();

		
		final TextButton cancelButton = new TextButton("CANCEL", skin, "default");
		
		cancelButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				openMainMenu();
			}
		});
		
		innerContainer.row().pad(10, 0, 0, 0);
		innerContainer.add(cancelButton).size(80f, 30f).left();
		
		
		final TextButton okButton = new TextButton("OK", skin, "default");
		
		okButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				int newLevelWidth = Integer.parseInt(levelSizeInput.getText());
				screen.optionsChanged(newLevelWidth);
				openMainMenu();
			}
		});
		
		innerContainer.add(okButton).size(80f, 30f).right();
		
		
		innerContainer.setBackground("default-window");
		container.add(innerContainer);
		stage.addActor(container);
		
		return container;
	}
	
	public void render() {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}
	
	public void dispose() {
		stage.dispose();
	}
	
	public void openMainMenu() {
		mainMenu.setVisible(true);
		saveMenu.setVisible(false);
		loadMenu.setVisible(false);
		optionsMenu.setVisible(false);
	}
	
	public void close() {
		mainMenu.setVisible(false);
		saveMenu.setVisible(false);
		loadMenu.setVisible(false);
		optionsMenu.setVisible(false);
	}
	
	public void updateFileName(String fileName) {
		fileNameInput.setText(fileName);
	}
	
	public void updateFileList(String fileName) {
		String[] files = LevelData.listLevelFiles();
		fileSelector.setItems(files);
		fileSelector.setSelected(fileName);
	}
}
