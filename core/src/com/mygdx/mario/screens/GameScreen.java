package com.mygdx.mario.screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.mario.GameScreenManager;
import com.mygdx.mario.Main;
import com.mygdx.mario.GameScreenManager.Screen;
import com.mygdx.mario.LevelData;

public abstract class GameScreen {
	protected OrthographicCamera camera;
	protected SpriteBatch spriteBatch;
	protected GameScreenManager gsm;
	protected AssetManager assetManager;
	protected Skin skin;
	
	public GameScreen(GameScreenManager gsm) {
		this.gsm = gsm;
		
		Main main = gsm.getMain();
		this.camera = main.getCamera();
		this.spriteBatch = main.getSpriteBatch();
		this.assetManager = main.getAssetManager();
		
		this.skin = assetManager.get("data/uiskin.json", Skin.class);
	}
	
	abstract public void render();
	abstract public void resize(int width, int height);
	
	public void setScreen(Screen screen) {
		gsm.setScreen(screen, null);
	}
	
	public void setScreen(Screen screen, LevelData levelData) {
		gsm.setScreen(screen, levelData);
	}
	
	public AssetManager getAssetManager() {
		return assetManager;
	}
	
	public OrthographicCamera getCamera() {
		return camera;
	}
}
