package com.mygdx.mario.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.mario.GameScreenManager;
import com.mygdx.mario.Hud;
import com.mygdx.mario.LevelData;
import com.mygdx.mario.World;
import com.mygdx.mario.GameScreenManager.Screen;

public class PlayScreen extends GameScreen implements InputProcessor {
	private float stateTime = 0f;
	private float timeInCurrentState = 0f;
	
	private World world;
	private Hud hud;
	
	private InputMultiplexer multiplexer;
	
	private Table menuWidget;
	private Table introScreen;
	private Label livesLabel;
	private Label worldLabel;
	private Table gameOverScreen;
	private Table continueScreen;
	
	private Sound gameOverSound;
	
	private Stage stage;
	
	
	public enum State {
		INTRO(2f),
		PLAYING(0),
		PAUSED(0),
		GAME_OVER(5f),
		REPLAY(0);
		
		float transitionTime;
		
		State (float time) {
			this.transitionTime = time;
		}
	};
	
	private State currentState;
	
	public PlayScreen(GameScreenManager gsm, LevelData levelData) {
		super(gsm);
		
		multiplexer = new InputMultiplexer();
		
		addInputProcessor(this);
		
		World.setLevelData(levelData);
		world = new World(this);
		hud = new Hud(world);
		
		stage = new Stage();
		addInputProcessor(stage);
		createMenuWidget();
		createIntroScreen();
		createGameOverScreen();
		createContinueScreen();
		
		showIntro();
		
		Gdx.input.setInputProcessor(multiplexer);
	}
	
	private void createMenuWidget() {
		menuWidget = new Table();
		stage.addActor(menuWidget);
		menuWidget.setFillParent(true);
		menuWidget.setVisible(false);
		
		final Table innerContainer = new Table(skin);
		innerContainer.pad(20).defaults().expandX().space(10);
		
		
		final TextButton resetButton = new TextButton("RESET", skin, "default");
		
		resetButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				menuWidget.setVisible(false);
				world.playMusic();
				currentState = State.PLAYING;
			}
		});
		
		innerContainer.row();
		innerContainer.add(resetButton).size(100f, 30f).pad(20, 0, 0, 0);
		
		
		final TextButton exitButton = new TextButton("EXIT", skin, "default");
		
		exitButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				world.stopMusic();
				setScreen(Screen.MAIN_MENU);
			}
		});
		
		innerContainer.row();
		innerContainer.add(exitButton).size(100f, 30f);
		
		innerContainer.setBackground("default-window");
		menuWidget.add(innerContainer);
	}
	
	private void createIntroScreen() {
		introScreen = new Table();
		stage.addActor(introScreen);
		
		introScreen.setFillParent(true);
		introScreen.setVisible(false);
		introScreen.pad(20).defaults().expandX().space(10);
		
		worldLabel = new Label("WOLD " + world.getLevelName(), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		livesLabel = new Label(String.format("LIVES: %02d", world.getLives()), new Label.LabelStyle(
				new BitmapFont(), Color.WHITE));
		
		introScreen.add(worldLabel).expandX().padTop(10);
		introScreen.row();
		introScreen.add(livesLabel).expandX();
		
		stage.addActor(introScreen);
	}
	
	private void createGameOverScreen() {
		gameOverSound = getAssetManager().get("sound/game-over.mp3", Sound.class);
		
		gameOverScreen = new Table();
		stage.addActor(gameOverScreen);
		
		gameOverScreen.setFillParent(true);
		gameOverScreen.setVisible(false);
		
		gameOverScreen.add(new Label("GAME OVER", new Label.LabelStyle(new BitmapFont(), Color.WHITE)))
			.expandX().padTop(10);
		
		stage.addActor(gameOverScreen);
	}
	
	private void createContinueScreen() {
		continueScreen = new Table();
		stage.addActor(continueScreen);
		
		continueScreen.setFillParent(true);
		continueScreen.setVisible(false);
		continueScreen.pad(20).defaults().expandX().space(10);
		
		continueScreen.add(new Label("CONTINUE?", new Label.LabelStyle(new BitmapFont(), Color.WHITE)))
			.expandX().padTop(10);
		
		
		final TextButton continueButton = new TextButton("YES", skin, "default");
		
		continueButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				world = new World(PlayScreen.this);
				hud.setWorld(world);
				showIntro();
			}
		});
		
		continueScreen.row();
		continueScreen.add(continueButton).size(100f, 30f);
		
		
		final TextButton abortButton = new TextButton("NO", skin, "default");
		
		abortButton.addListener(new ClickListener() {
			@Override 
			public void clicked(InputEvent event, float x, float y) {
				setScreen(Screen.MAIN_MENU);
			}
		});
		
		continueScreen.row();
		continueScreen.add(abortButton).size(100f, 30f);
		
		stage.addActor(gameOverScreen);
	}
	
	public void update(float deltaTime) {
		timeInCurrentState += deltaTime;
		
		switch(currentState) {
			case INTRO:
				livesLabel.setText(String.format("LIVES: %02d", world.getLives()));
				worldLabel.setText("WORLD: " + world.getLevelName());
				if (timeInCurrentState >= currentState.transitionTime) {
					startPlaying();
				}
				break;
			case GAME_OVER:
				if (timeInCurrentState >= currentState.transitionTime) {
					showReplay();
				}
				break;
			case PLAYING:
				world.update(deltaTime);
				hud.update(deltaTime);
				break;
			default:
				break;
		}
	}
	
	public void render() {
		float deltaTime = Gdx.graphics.getDeltaTime();
		if (deltaTime > 0.05) {
			deltaTime = 0.05f;
		}
		
		update(deltaTime);
		
		stateTime += deltaTime;
		
		switch(currentState) {
			case INTRO:
			case GAME_OVER:
			case REPLAY:
				Gdx.gl.glClearColor(0, 0, 0, 0);
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				break;
			case PAUSED:
			case PLAYING:
				world.render(spriteBatch, stateTime);
				break;
			default:
				break;
		}
		
		hud.render();
		
		stage.act(deltaTime);
		stage.draw();
	}
	
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
		hud.resize(width, height);
	}
	
	public void dispose() {
		stage.dispose();
		hud.dispose();
		world.dispose();
	}
	
	public void addInputProcessor(InputProcessor processor) {
		multiplexer.addProcessor(processor);
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.ESCAPE) {
			switch (currentState) {
				case PLAYING:
					menuWidget.setVisible(true);
					world.pauseMusic();
					currentState = State.PAUSED;
					break;
				case PAUSED:
					menuWidget.setVisible(false);
					world.playMusic();
					currentState = State.PLAYING;
					break;
				default:
					break;
			}
        }
		
        return true;
	}
	
	public void showIntro() {
		timeInCurrentState = 0;
		continueScreen.setVisible(false);
		currentState = State.INTRO;
		introScreen.setVisible(true);
	}
	
	public void startPlaying() {
		timeInCurrentState = 0;
		introScreen.setVisible(false);
		currentState = State.PLAYING;
		world.playMusic();
	}
	
	public void levelFinished() {
		showIntro();
	}
	
	public void worldFinished() {
		System.out.println("There are no more levels in this world");
		setScreen(Screen.MAIN_MENU);
	}
	
	public void gameOver() {
		timeInCurrentState = 0;
		gameOverSound.play();
		currentState = State.GAME_OVER;
		gameOverScreen.setVisible(true);
	}
	
	public void showReplay() {
		gameOverScreen.setVisible(false);
		currentState = State.REPLAY;
		continueScreen.setVisible(true);
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
