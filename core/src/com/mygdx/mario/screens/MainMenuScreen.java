package com.mygdx.mario.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.mario.GameScreenManager;
import com.mygdx.mario.GameScreenManager.Screen;

public class MainMenuScreen extends GameScreen {
	private Stage stage;
	 
	public MainMenuScreen(GameScreenManager gsm) {
		super(gsm);
		
		stage = new Stage();
		
		final TextButton playButton = new TextButton("PLAY", skin, "default");
		
		playButton.setWidth(200f);
		playButton.setHeight(30f);
		playButton.setPosition(Gdx.graphics.getWidth() /2 - 100f, Gdx.graphics.getHeight()/2 + 50f);
		
		playButton.addListener(new ClickListener(){
			@Override 
			public void clicked(InputEvent event, float x, float y){
				setScreen(Screen.PLAY);
			}
		});
		
		final TextButton editButton = new TextButton("EDITOR", skin, "default");
		
		editButton.setWidth(200f);
		editButton.setHeight(30f);
		editButton.setPosition(Gdx.graphics.getWidth() /2 - 100f, Gdx.graphics.getHeight()/2);
		
		editButton.addListener(new ClickListener(){
			@Override 
			public void clicked(InputEvent event, float x, float y){
				setScreen(Screen.EDITOR);
			}
		});
		
		stage.addActor(playButton);
		stage.addActor(editButton);
		
		Gdx.input.setInputProcessor(stage);
	}
	
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		spriteBatch.begin();
		stage.draw();
		spriteBatch.end();
	}
	
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}
	
	public void dispose() {
		
	}
}
