package com.mygdx.mario.screens;


import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.mario.Canvas;
import com.mygdx.mario.GameScreenManager;
import com.mygdx.mario.LevelData;
import com.mygdx.mario.GameScreenManager.Screen;
import com.mygdx.mario.screens.editorWidgets.Menu;
import com.mygdx.mario.screens.editorWidgets.ThingPicker;

public class EditorScreen extends GameScreen implements InputProcessor {
	private float stateTime = 0f;
	private Canvas canvas;
	private InputMultiplexer multiplexer;
	
	private Menu menuWidget;
	private ThingPicker tilePicker;
	
	public enum State {
		EDITING,
		PAUSED;
	};
	
	private State currentState;
	
	public EditorScreen(GameScreenManager gsm) {
		super(gsm);
		
		multiplexer = new InputMultiplexer();
		addInputProcessor(this);
		
		currentState = State.EDITING;
		
		tilePicker = new ThingPicker(this);
		canvas = new Canvas(this);
		menuWidget = new Menu(this);
		
		Gdx.input.setInputProcessor(multiplexer);
	}

	public void render() {
		if (currentState == State.EDITING) {
			stateTime += Gdx.graphics.getDeltaTime();
		}
		
		canvas.render(spriteBatch, stateTime);
		tilePicker.render();
		menuWidget.render();
	}
	
	public void resize (int width, int height) {
		menuWidget.resize(width, height);
		tilePicker.resize(width, height);
	}
	
	public void dispose() {
		menuWidget.dispose();
		tilePicker.dispose();
	}
	
	public void addInputProcessor(InputProcessor processor) {
		multiplexer.addProcessor(processor);
	}
	
	public ThingPicker getTilePicker() {
		return tilePicker;
	}
	
	public State getState() {
		return currentState;
	}
	
	public Skin getSkin() {
		return skin;
	}
	
	public String getFileName() {
		return canvas.getLevelName();
	}

	public void closeEditor() {
		setScreen(Screen.MAIN_MENU);
	}
	
	public void playLevel() {
		LevelData lvlData = canvas.getLevelData();
		if (lvlData != null) {
			setScreen(Screen.PLAY, canvas.getLevelData());
		} else {
			System.out.println("You have to create a Mario object!");
		}
	}
	
	public void loadLevel(String fileName) {
		try {
			canvas.setLevelData(LevelData.loadLevel(fileName));
			currentState = State.EDITING;
			menuWidget.updateFileName(fileName);
			menuWidget.close();
			tilePicker.enableInputs();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveLevel(String fileName) {
		LevelData lvlData = canvas.getLevelData();
		if (lvlData != null) {
			try {
				lvlData.saveLevel(fileName);
				canvas.setLevelName(fileName);
				menuWidget.updateFileList(fileName);
				menuWidget.openMainMenu();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("You have to create a Mario object!");
		}
	}
	
	public void optionsChanged(int newLevelWidth) {
		canvas.changeLevelWidth(newLevelWidth);
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.ESCAPE) {
			switch (currentState) {
				case EDITING:
					menuWidget.openMainMenu();
					currentState = State.PAUSED;
					tilePicker.disableInputs();
					break;
				case PAUSED:
					menuWidget.close();
					currentState = State.EDITING;
					tilePicker.enableInputs();
					break;
				default:
					break;
			}
        }
		
        return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
