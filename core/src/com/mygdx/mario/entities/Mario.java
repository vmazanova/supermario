package com.mygdx.mario.entities;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.Tile.InteractionDirection;
import com.mygdx.mario.entities.enemies.Enemy;

public class Mario extends Entity {
	private static float TRANSFORM_TIME = 1f;
	private static float INVINCIBLE_TIME = 2f;
	
	public enum State {
		STANDING,
		RUNNING,
		JUMPING,
		GROWING,
		SHRINKING,
		DEAD,
		CLIMBING,
		SITTING;
		
		TextureRegion texture;
		TextureRegion bigMarioTexture;
		Animation animation;
		Animation bigMarioAnimation;
		Sound sound;
		Sound bigMarioSound;
	};
	
	private State currentState;
	
	private boolean runningRight = true;
	private boolean isBig = false;
	private boolean isInvincible = false;
	
	private boolean touchedGroundSinceLastStomp = false;
	
	private float transformTimeLeft = TRANSFORM_TIME;
	private float invincibleTimeLeft = INVINCIBLE_TIME;
	
	public Mario(Vector2 position, Level level) {
		super(Type.MARIO, position, new Vector2(0, 0), new Vector2(1, 1), level);
		
		this.canCollideWith.add(Entity.Type.COIN);
		this.canCollideWith.add(Entity.Type.GOOMBA);
		this.canCollideWith.add(Entity.Type.KOOPA);
		this.canCollideWith.add(Entity.Type.MAGIC_MUSHROOM);
		this.canCollideWith.add(Entity.Type.UP_MUSHROOM);
		
		currentState = State.STANDING;
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture marioSheet = assetManager.get("mario_sheet.png", Texture.class);
		
		TextureRegion[] runningAnimation = new TextureRegion[4];
		TextureRegion[] bigMarioRunningAnimation = new TextureRegion[4];
		for (int j = 0; j < 4; j++) {
			runningAnimation[j] = new TextureRegion(marioSheet, j*32, 3*32, 32, 32);
			bigMarioRunningAnimation[j] = new TextureRegion(marioSheet, j*32, 0*32, 32, 64);
		}
		
		TextureRegion[] climbingAnimation = new TextureRegion[2];
		TextureRegion[] bigMarioClimbingAnimation = new TextureRegion[2];
		for (int j = 7; j < 9; j++) {
			climbingAnimation[j - 7] = new TextureRegion(marioSheet, j*32, 3*32, 32, 32);
			bigMarioClimbingAnimation[j - 7] = new TextureRegion(marioSheet, j*32, 0*32, 32, 64);
		}
		
		TextureRegion[] growingAnimation = new TextureRegion[2];
		growingAnimation[0] = new TextureRegion(marioSheet, 15*32, 0*32, 32, 64);
		growingAnimation[1] = new TextureRegion(marioSheet, 0*32, 0*32, 32, 64);
		
		State.STANDING.texture = new TextureRegion(marioSheet, 0, 32*3, 32, 32);
		State.STANDING.bigMarioTexture = new TextureRegion(marioSheet, 0*32, 0*32, 32, 64);
		State.RUNNING.animation = new Animation(0.15f, runningAnimation);
		State.RUNNING.bigMarioAnimation = new Animation(0.15f, bigMarioRunningAnimation);
		State.JUMPING.texture = new TextureRegion(marioSheet, 5*32, 3*32, 32, 32);
		State.JUMPING.bigMarioTexture = new TextureRegion(marioSheet, 5*32, 0*32, 32, 64);
		State.GROWING.animation = new Animation(0.17f, growingAnimation);
		State.SHRINKING.bigMarioAnimation = new Animation(0.17f, growingAnimation);
		State.DEAD.texture = new TextureRegion(marioSheet, 6*32, 3*32, 32, 32);
		State.SITTING.texture = new TextureRegion(marioSheet, 7*32, 3*32, 32, 32);
		State.SITTING.bigMarioTexture = new TextureRegion(marioSheet, 8*32, 0*32, 32, 64);
		State.CLIMBING.animation = new Animation(0.15f, climbingAnimation);
		State.CLIMBING.bigMarioAnimation = new Animation(0.15f, bigMarioClimbingAnimation);
		
		State.DEAD.sound = assetManager.get("sound/mario-die.wav", Sound.class);
		State.JUMPING.sound = assetManager.get("sound/jump-small.wav", Sound.class);
		State.JUMPING.bigMarioSound = assetManager.get("sound/jump-super.wav", Sound.class);
		State.SHRINKING.sound = assetManager.get("sound/shrink.wav", Sound.class);
	}
	
	public static TextureRegion getBaseTexture() {
		return State.STANDING.texture;
	}
	
	public void update(float time) {
		if (isTransforming()) {
			if (transformTimeLeft > 0) {
				transformTimeLeft -= time;
			} else {
				if (currentState == State.GROWING) {
					isBig = true;
					size.y = 2f;
				} else {
					isBig = false;
					size.y = 1f;
				}
				
				currentState = State.STANDING;
				transformTimeLeft = TRANSFORM_TIME;
			}
			
			return;
		}
		
		if (level.isOutOfLevel(position)) {
			die();
		}
		
		velocity.y -= GRAVITY * time;
		
		if (velocity.x > 0) {
			velocity.x -= 62.5f * time;
			if (velocity.x <= 0) {
				velocity.x = 0;
			}
		} else {
			velocity.x += 62.5f * time;
			if (velocity.x >= 0) {
				velocity.x = 0;
			}
		}
		
		if (isInvincible) {
			if (invincibleTimeLeft > 0) {
				invincibleTimeLeft -= time;
			} else {
				isInvincible = false;
				invincibleTimeLeft = INVINCIBLE_TIME;
			}
		}
		
		if (!isDead()) {
			checkTileCollisions(this, time);
			
			if (velocity.y != 0 && currentState != State.CLIMBING && currentState != State.SITTING) {
				currentState = State.JUMPING;
			}
			
		    if (velocity.x == 0 && currentState == State.RUNNING) {
		    	currentState = State.STANDING;
		    }
		}
		
		position.x += velocity.x * time;
		position.y += velocity.y * time;
	}
	
	public void render(SpriteBatch spriteBatch, float stateTime) {
		int marioHeight = 1;
		if (isBig || isTransforming()) {
			marioHeight = 2;
		}
		
		if (isInvincible) {
			Color c = spriteBatch.getColor();
			spriteBatch.setColor(c.r, c.g, c.b, 0.5f);
			spriteBatch.draw(getTexture(stateTime), position.x, position.y, 1, marioHeight);
			spriteBatch.setColor(c.r, c.g, c.b, 1.0f);
		} else {
			spriteBatch.draw(getTexture(stateTime), position.x, position.y, 1, marioHeight);
		}
	}
	
	public TextureRegion getTexture(float stateTime) {
		TextureRegion currentTexture = currentState.texture;
		if (isBig) {
			if (currentState.bigMarioAnimation != null) {
				currentTexture = currentState.bigMarioAnimation.getKeyFrame(stateTime, true);
			} else if (currentState.bigMarioTexture != null) {
				currentTexture = currentState.bigMarioTexture;
			}
		} else if (currentState.animation != null) {
			currentTexture = currentState.animation.getKeyFrame(stateTime, true);
		}
		
		if (runningRight && currentTexture.isFlipX() || !runningRight && !currentTexture.isFlipX())
			currentTexture.flip(true, false);
		
		return currentTexture;
	}
	
	public boolean touchedGroundSinceLastStomp() {
		return touchedGroundSinceLastStomp;
	}
	
	public void setVelocity(Vector2 newVelocity) {
		velocity.x = newVelocity.x;
		velocity.y = newVelocity.y;
	}
	
	public Vector2 getVelocity() {
		return velocity;
	}
	
	public void moveLeft() {
		if (isTransforming()) {
			return;
		}
		
		velocity.x = -6.25f;
		runningRight = false;
		
		if (currentState != State.JUMPING) {
			currentState = State.RUNNING;
		}
	}
	
	public void moveRight() {
		if (isTransforming()) {
			return;
		}
		
		velocity.x = 6.25f;
		runningRight = true;
		
		if (currentState != State.JUMPING) {
			currentState = State.RUNNING;
		}
	}
	
	public void jump() {
		if (currentState == State.JUMPING || isTransforming()) {
			return;
		}
		
		velocity.y = 15.625f;
		currentState = State.JUMPING;
		if (isBig && currentState.bigMarioSound != null) {
			currentState.bigMarioSound.play();
		} else {
			currentState.sound.play();
		}
	}
	
	public void finishJump() {
		if (currentState == State.JUMPING && velocity.y > GRAVITY / 3) {
			velocity.y = GRAVITY / 3;
		}
	}
	
	public void die() {
		if (isDead()) {
			return;
		}
		
		velocity.y = 10f;
		currentState = State.DEAD;
		isBig = false;
		size.y = 1f;
		
		level.marioDied();
		currentState.sound.play();
	}
	
	public void startClimbing() {
		currentState = State.CLIMBING;
		velocity.y = 0f;
	}
	
	public void growUp() {
		if (!isBig) {
			currentState = State.GROWING;
		}
	}
	
	public boolean isBig() {
		return isBig;
	}
	
	public boolean isTransforming() {
		return (currentState == State.GROWING || currentState == State.SHRINKING);
	}
	
	public void collideWithTile(InteractionDirection dir) {
		switch (dir) {
			case DOWN:
				switch(currentState) {
					case JUMPING:
						currentState = State.STANDING;
						touchedGroundSinceLastStomp = true;
						break;
					case CLIMBING:
						currentState = State.SITTING;
						break;
					default:
						break;
				}
				break;
			case LEFT:
			case RIGHT:
				velocity.x = 0;
				break;
			default:
				break;
		}
	}
	
	public void hitByEnemy() {
		if (isInvincible) {
			return;
		}
		
		if (isBig) {
			currentState = State.SHRINKING;
			currentState.sound.play();
			isInvincible = true;
		} else {
			die();
		}
	}
	
	public void stomp(Enemy enemy) {
		if (velocity.y > 0) {
			return;
		}
		
		enemy.stompedByMario(this);
	}
	
	public void bounce() {
		velocity.y = -1 * velocity.y / 1.3f;
		touchedGroundSinceLastStomp = false;
	}
	
	public void kick(Enemy enemy) {
		enemy.kickedByMario(this);
	}
	
	public boolean isDead() {
		return currentState == State.DEAD;
	}
	
	public void collideWithMovingTile(Vector2 tilePosition) {
		// this situation should not occour
	}
}
