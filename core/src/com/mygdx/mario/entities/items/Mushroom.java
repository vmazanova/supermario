package com.mygdx.mario.entities.items;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;

public abstract class Mushroom extends Item {
	protected static Sound appearanceSound;
	public enum State {
		RISING,
		MOVING;
	};
	
	private State currentState;
	
	protected float startingPositionY;

	public Mushroom(Entity.Type type, Vector2 position, Level level, TextureRegion texture) {
		super(type, new Vector2(position.x, position.y + 0.3f), new Vector2(0, 1f), level, texture);
		
		this.canCollideWith.add(Entity.Type.MARIO);
		
		startingPositionY = position.y;
		currentState = State.RISING;
		appearanceSound.play();
		isAtBackground = true;
	}
	
	public static void loadAssets(AssetManager assetManager) {
		appearanceSound = assetManager.get("sound/powerup-appears.wav", Sound.class);
	}
	
	public void update(float deltaTime) {
		if (level.isOutOfLevel(position)) {
			isAlive = false;
			return;
		}
		
		if (currentState == State.RISING) {
			position.y += velocity.y * deltaTime;
			if (position.y >= startingPositionY + 1f) {
				currentState = State.MOVING;
				velocity = new Vector2(3.25f, 0);
				isAtBackground = false;
			}
		}
		
		if (currentState == State.MOVING) {
			velocity.y -= GRAVITY * deltaTime;
			
			if (!isDead()) {
				checkTileCollisions(this, deltaTime);
			}
			
			position.x += velocity.x * deltaTime;
			position.y += velocity.y * deltaTime;
		}
	}
	
	public void collideWithMovingTile(Vector2 tilePosition) {
		if (currentState != State.MOVING) {
			return;
		}
		
		velocity.y = 8f;
		
		if (position.x < tilePosition.x) {
			if (velocity.x > 0) {
				velocity.x *= -1;
			}
		} else if (velocity.x < 0) {
			velocity.x *= -1;
		}
	}
}
