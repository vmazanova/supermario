package com.mygdx.mario.entities.items;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public class Coin extends Item {
	private static Animation animation;
	private static TextureRegion texture;
	
	public Coin(Vector2 position, Level level) {
		super(Entity.Type.COIN, position, new Vector2(3.25f, 0), level, animation);
		
		this.canCollideWith.add(Entity.Type.MARIO);
	}

	public static void loadAssets(AssetManager assetManager) {
		Texture itemsSheet = assetManager.get("items_sheet.png", Texture.class);
		
		TextureRegion[] coinAnimation = new TextureRegion[4];
		for (int j = 0; j < 4; j++) {
			coinAnimation[j] = new TextureRegion(itemsSheet, j*16, 6*16, 16, 16);
		}
		
		animation = new Animation(0.25f, coinAnimation);
		texture = coinAnimation[0];
	}
	
	public static TextureRegion getBaseTexture() {
		return texture;
	}
	
	public void update(float deltaTime) {
		
	}

	public void use(Mario mario) {
		if (!isAlive) {
			return;
		}
		
		isAlive = false;
		level.addCoin();
	}
	
	public void collideWithMovingTile(Vector2 tilePosition) {
		level.spawnCoinEffect(position);
		isAlive = false;
	}
}
