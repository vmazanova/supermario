package com.mygdx.mario.entities.items;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public class MagicMushroom extends Mushroom {
	private static TextureRegion texture;
	private static Sound pickupSound;
	
	private int POINTS = 1000;
	
	public MagicMushroom(Vector2 position, Level level) {
		super(Entity.Type.MAGIC_MUSHROOM, position, level, texture);
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture itemsSheet = assetManager.get("items_sheet.png", Texture.class);
		
		texture = new TextureRegion(itemsSheet, 0*16, 0*16, 16, 16);
		pickupSound = assetManager.get("sound/powerup-pickup.wav", Sound.class);
	}

	public static TextureRegion getBaseTexture() {
		return texture;
	}

	public void use(Mario mario) {
		if (!isAlive) {
			return;
		}
		
		pickupSound.play();
		isAlive = false;
		mario.growUp();
		level.addScoreWithNotification(POINTS, position);
	}

}
