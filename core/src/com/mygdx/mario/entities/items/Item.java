package com.mygdx.mario.entities.items;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public abstract class Item extends Entity{
	protected TextureRegion texture;
	protected Animation animation;
	
	protected boolean isAlive = true;
	protected boolean isAtBackground = false;
	
	public enum Type {
		COIN(30),
		UP_MUSHROOM(31),
		MAGIC_MUSHROOM(32);
		
		public int id;
		
		Type (int id) {
			this.id = id;
		}
	}
	
	public Item(Entity.Type type, Vector2 position, Vector2 velocity, Level level, Animation animation) {
		this(type, position, velocity, level);
	
		this.animation = animation;
	}
	
	public Item(Entity.Type type, Vector2 position, Vector2 velocity, Level level, TextureRegion texture) {
		this(type, position, velocity, level);
	
		this.texture = texture;
	}
	
	public Item(Entity.Type type, Vector2 position, Vector2 velocity, Level level) {
		super(type, position, velocity, new Vector2(1, 1), level);
	}

	public TextureRegion getTexture(float stateTime) {
		if (animation != null) {
			return animation.getKeyFrame(stateTime, true);
		}
		
		return texture;
	}
	
	public boolean isDead() {
		return false;
	}
	
	public boolean shouldBeRemoved() {
		return !isAlive;
	}
	
	public boolean isAtBackground() {
		return isAtBackground;
	}

	public abstract void use(Mario mario);
	public abstract void update(float deltaTime);
}
