package com.mygdx.mario.entities.items;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public class UpMushroom extends Mushroom {
	private static TextureRegion texture;
	
	public UpMushroom(Vector2 position, Level level) {
		super(Entity.Type.UP_MUSHROOM, position, level, texture);
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture itemsSheet = assetManager.get("items_sheet.png", Texture.class);
		
		texture = new TextureRegion(itemsSheet, 1*16, 0*16, 16, 16);
	}

	public static TextureRegion getBaseTexture() {
		return texture;
	}

	public void use(Mario mario) {
		if (!isAlive) {
			return;
		}
		
		isAlive = false;
		level.addLife();
		level.notifyReward(position, "1UP");
	}
}
