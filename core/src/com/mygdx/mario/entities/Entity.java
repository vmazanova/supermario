package com.mygdx.mario.entities;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.Tile;
import com.mygdx.mario.Tile.InteractionDirection;

public abstract class Entity {
	protected Vector2 position, velocity;
	public Vector2 size;
	protected static float GRAVITY = 28.125f;
	protected Level level;
	protected boolean isSpawned = false;
	
	public enum Type {
		MARIO(0),
		GOOMBA(5),
		KOOPA(6),
		COIN(30),
		UP_MUSHROOM(31),
		MAGIC_MUSHROOM(32);
		
		public int id;
		
		Type (int id) {
			this.id = id;
		}
	}
	
	protected Type type;
	
	protected List<Type> canCollideWith = new ArrayList<Type>();
	
	public Entity(Type type, Vector2 position, Vector2 velocity, Vector2 size, Level level) {
		this.type = type;
		this.position = position;
		this.velocity = velocity;
		this.size = size;
		this.level = level;
	}
	
	public Vector2 getPosition() {
		return position;
	}
	
	public Vector2 getVelocity() {
		return velocity;
	}
	
	public void setPosition(Vector2 position) {
		this.position = position;
	}
	
	public Type getType() {
		return type;
	}
	
	public Vector2 getSize() {
		return size;
	}
	
	public void setLevel(Level level) {
		this.level = level;
	}
	
	private List<Tile> getInteractingTiles(List<Vector2> positionsToCheck) {
		List<Tile> interactingTiles = new ArrayList<Tile>();
		for (Vector2 pos: positionsToCheck) {
			Tile possibleTile = level.getTile(pos.x, pos.y);
			Boolean canBeAdded = true;
			for (Tile iTile: interactingTiles) {
				if (iTile == possibleTile) {
					canBeAdded = false;
				}
			}
			if (canBeAdded) {
				interactingTiles.add(possibleTile);
			}
		}
		
		return interactingTiles;
	}
	
	public void checkTileCollisions(Entity entity, float deltaTime) {
		Vector2 position = entity.getPosition();
		Vector2 velocity = entity.getVelocity();
		
		Vector2 nextPosition = new Vector2(position.x + velocity.x * deltaTime, position.y + velocity.y * deltaTime);
		
		List<Vector2> positionsToCheck = new ArrayList<Vector2>();
		List<Tile> interactingTiles = new ArrayList<Tile>();
		
		// solid tile above entity
		if (velocity.y > 0) {
			positionsToCheck.add(new Vector2(position.x + 0.50f, nextPosition.y + entity.getSize().y - 0.99f));
			positionsToCheck.add(new Vector2(position.x, nextPosition.y + entity.getSize().y - 0.99f));
			positionsToCheck.add(new Vector2(position.x + 0.99f, nextPosition.y + entity.getSize().y - 0.99f));
			
			interactingTiles = getInteractingTiles(positionsToCheck);

			Boolean collisionDetected = false;
			for (Tile iTile: interactingTiles) {
				if (!collisionDetected) {
					if (iTile.isTileBlocking()) {
						collisionDetected = true;
					}
					iTile.interact(entity, InteractionDirection.UP);
				}
			}
			
			if (collisionDetected) {
				velocity.y = 0;
			}
			
			positionsToCheck.clear();
			interactingTiles.clear();
		}
		
		// solid tile below mario
		if (velocity.y < 0) {
			positionsToCheck.add(new Vector2(position.x + 0.99f, nextPosition.y - 0.99f));
			positionsToCheck.add(new Vector2(position.x, nextPosition.y - 0.99f));
			
			interactingTiles = getInteractingTiles(positionsToCheck);

			Boolean collisionDetected = false;
			for (Tile iTile: interactingTiles) {
				if (!collisionDetected) {
					iTile.interact(entity, InteractionDirection.DOWN);
					if (iTile.isTileBlocking()) {
						collisionDetected = true;
					}
				}
			}
			
			if (collisionDetected) {
				// get entity position with resolved collision
				Vector2 newPos = level.getTileCoords(position.x, nextPosition.y);
				if (newPos != null) {
					if (newPos.y <= position.y) {
						position.y = newPos.y;
					}
					
					velocity.y = 0;
					
					entity.collideWithTile(InteractionDirection.DOWN);
				}
			}
			
			positionsToCheck.clear();
			interactingTiles.clear();
		}
		
		// solid tile on the right side of entity
		if (velocity.x > 0) {
			positionsToCheck.add(new Vector2(nextPosition.x + 0.99f, position.y - 0.99f));
			positionsToCheck.add(new Vector2(nextPosition.x + 0.99f, position.y));
			if (entity.getSize().y > 1) {
				positionsToCheck.add(new Vector2(nextPosition.x + 0.99f, position.y + 0.99f));
			}
			
			interactingTiles = getInteractingTiles(positionsToCheck);
			
			Boolean collisionDetected = false;
			for (Tile iTile: interactingTiles) {
				if (!collisionDetected) {
					iTile.interact(entity, InteractionDirection.RIGHT);
					if (iTile.isTileBlocking()) {
						collisionDetected = true;
					}
				}
			}
			
			if (collisionDetected) {
				Vector2 newPos = level.getTileCoords(nextPosition.x, position.y);
				if (newPos != null) {
					position.x = newPos.x;
					entity.collideWithTile(InteractionDirection.RIGHT);
				}
			}
			
			positionsToCheck.clear();
			interactingTiles.clear();
		}
		
		// solid tile on the left side of entity
		if (velocity.x < 0) {
			positionsToCheck.add(new Vector2(nextPosition.x, position.y - 0.99f));
			positionsToCheck.add(new Vector2(nextPosition.x, position.y));
			if (entity.getSize().y > 1) {
				positionsToCheck.add(new Vector2(nextPosition.x, position.y + 0.99f));
			}
			
			interactingTiles = getInteractingTiles(positionsToCheck);
			
			Boolean collisionDetected = false;
			for (Tile iTile: interactingTiles) {
				if (!collisionDetected) {
					iTile.interact(entity, InteractionDirection.LEFT);
					if (iTile.isTileBlocking()) {
						collisionDetected = true;
					}
				}
			}
			
			if (collisionDetected) {
				Vector2 newPos = level.getTileCoords(nextPosition.x + 0.99f, position.y);
				if (newPos != null) {
					position.x = newPos.x;
					entity.collideWithTile(InteractionDirection.LEFT);
				}
			}
			
			positionsToCheck.clear();
			interactingTiles.clear();
		}
	}
	
	public boolean canCollideWith(Entity ent) {
		return canCollideWith.contains(ent.getType());
	}
	
	public void collideWithTile(InteractionDirection dir) {
		switch (dir) {
			case RIGHT:
			case LEFT:
				velocity.x *= -1;
				break;
			default:
				break;
		}
	}
	
	public abstract void update(float time);
	public abstract TextureRegion getTexture(float stateTime);
	public abstract boolean isDead();
	public abstract void collideWithMovingTile(Vector2 tilePosition);
}
