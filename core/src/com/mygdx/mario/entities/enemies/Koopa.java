package com.mygdx.mario.entities.enemies;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public class Koopa extends Enemy {

	public enum State {
		MOVING,
		SHELL,
		MOVING_SHELL,
		FALLING;
		
		TextureRegion texture;
		Animation animation;
		Sound sound;
		Vector2 size;
		Rectangle head;
		Rectangle body;
	};
	
	private static int DEFAULT_POINTS = 100;
	private static int KICK_SHELL_POINTS = 400;
	private static int STOMP_SHELL_POINTS = 500;
	
	private State currentState;
	
	private float timeToShell = 3f;
	private float timeToIgnoreMario = 0.1f;

	public Koopa(Vector2 position, Level level) {
		super(Type.KOOPA, position, new Vector2(-3.25f, 0), new Vector2(1, 2f), level);
		
		this.canCollideWith.add(Entity.Type.KOOPA);
		this.canCollideWith.add(Entity.Type.GOOMBA);
		this.canCollideWith.add(Entity.Type.MARIO);
		
		currentState = State.MOVING;
	}

	public static void loadAssets(AssetManager assetManager) {
		Texture enemiesSheet = assetManager.get("enemies_sheet.png", Texture.class);
		
		TextureRegion[] movingAnimation = new TextureRegion[2];
		for (int j = 0; j < 2; j++) {
			movingAnimation[j] = new TextureRegion(enemiesSheet, (j+6)*16, 0*32, 16, 32);
		}
		
		State.MOVING.texture = new TextureRegion(enemiesSheet, 6*16, 0*32, 16, 16);
		State.MOVING.animation = new Animation(0.15f, movingAnimation);
		State.SHELL.texture = new TextureRegion(enemiesSheet, 10*16, 0*32+16, 16, 16);
		State.MOVING_SHELL.texture = new TextureRegion(enemiesSheet, 10*16, 0*32+16, 16, 16);
		State.FALLING.texture = new TextureRegion(enemiesSheet, 10*16, 0*32+16, 16, 16);
		State.FALLING.texture.flip(false, true);
		
		State.SHELL.sound = assetManager.get("sound/stomp.wav", Sound.class);
		State.MOVING_SHELL.sound = assetManager.get("sound/kick.wav", Sound.class);
		State.FALLING.sound = assetManager.get("sound/kick.wav", Sound.class);
		
		State.MOVING.size = new Vector2(1, 2f);
		State.MOVING.head = new Rectangle(0, 10f/16, 16f/16, 10f/16);
		State.MOVING.body = new Rectangle(0, 0, 16f/16, 10f/16);
		State.SHELL.size = new Vector2(1, 1);
		State.SHELL.head = new Rectangle(0, 8f/16, 16f/16, 8f/16);
		State.SHELL.body = new Rectangle(0, 0, 16f/16, 8f/16);
		State.MOVING_SHELL.size = new Vector2(1, 1);
		State.MOVING_SHELL.head = new Rectangle(0, 8f/16, 16f/16, 8f/16);
		State.MOVING_SHELL.body = new Rectangle(0, 0, 16f/16, 8f/16);
		State.FALLING.size = new Vector2(1, 1);
	}
	
	public static TextureRegion getBaseTexture() {
		return State.MOVING.texture;
	}
	
	public Vector2 getSize() {
		return currentState.size;
	}
	
	public Rectangle getHead() {
		return currentState.head;
	}
	
	public Rectangle getBody() {
		return currentState.body;
	}
	
	public boolean isMovingShell() {
		return currentState.equals(State.MOVING_SHELL);
	}
	
	private void changeState(State newState) {
		currentState = newState;
		
		switch(currentState) {
			case MOVING:
				size.y = 2f;
				velocity.x = 3.25f * (velocity.x < 0 ? -1 : 1);
				break;
			case SHELL:
				timeToShell = 3f;
				break;
			case MOVING_SHELL:
				timeToIgnoreMario = 0.1f;
				break;
			default:
				size.y = 1f;
		}
	}
	
	public void update(float deltaTime) {
		if (currentState == State.SHELL) {
			timeToShell -= deltaTime;
			
			if (timeToShell <= 0) {
				changeState(State.MOVING);
			}
			
			return;
		} else if (currentState == State.MOVING_SHELL && timeToIgnoreMario >= 0) {
			timeToIgnoreMario -= deltaTime;
		}
		
		if (level.isOutOfLevel(position)) {
			changeState(State.FALLING);
		}
		
		velocity.y -= GRAVITY * deltaTime;
		
		if (!isDead()) {
			checkTileCollisions(this, deltaTime);
		}
		
		position.x += velocity.x * deltaTime;
		position.y += velocity.y * deltaTime;
	}
	
	public TextureRegion getTexture(float stateTime) {
		TextureRegion currentTexture;
		Animation stateAnimation = currentState.animation;
		if (stateAnimation != null) {
			currentTexture = stateAnimation.getKeyFrame(stateTime, true);
		} else {
			currentTexture = currentState.texture;
		}
		
		if (velocity.x > 0 && !currentTexture.isFlipX() || velocity.x <= 0 && currentTexture.isFlipX())
			currentTexture.flip(true, false);
		
		return currentTexture;
	}
	
	public void hit(Enemy enemy) {
		switch (currentState) {
			case MOVING_SHELL:
				enemy.hitByShell();
				break;
			case MOVING:
				if (enemy.getPosition().x > position.x == velocity.x > 0) {
					velocity.x *= -1;
				}
				break;
			default:
				break;
		}
	}

	public void stompedByMario(Mario mario) {
		switch (currentState) {
			case SHELL:
				if (mario.touchedGroundSinceLastStomp()) {
					level.addScoreWithNotification(KICK_SHELL_POINTS, position);
				} else {
					level.addScoreWithNotification(STOMP_SHELL_POINTS, position);
				}
				
				moveShell(mario);
				break;
			case MOVING_SHELL:
				if (timeToIgnoreMario > 0) {
					return;
				}
			default:
				changeState(State.SHELL);
				level.addScoreWithNotification(DEFAULT_POINTS, position);
		}
		
		currentState.sound.play();
		mario.bounce();
	}
	
	private void moveShell(Mario mario) {
		changeState(State.MOVING_SHELL);
		
		if (position.x < mario.getPosition().x) {
			if (velocity.x > 0) {
				velocity.x *= -1;
			}
		} else if (velocity.x < 0) {
			velocity.x *= -1;
		}
		velocity.x = 8 * (velocity.x < 0 ? -1 : 1);
	}

	public void kickedByMario(Mario mario) {
		if (currentState.equals(State.MOVING_SHELL) && timeToIgnoreMario > 0) {
			return;
		}
		
		if (currentState.equals(State.SHELL)) {
			level.addScoreWithNotification(KICK_SHELL_POINTS, position);		
			moveShell(mario);
			
			currentState.sound.play();
		} else {
			mario.hitByEnemy();
		}
	}

	public void fall() {
		currentState = State.FALLING;
		currentState.sound.play();
	}
	
	public void hitFromBellow() {
		fall();
		level.addScoreWithNotification(DEFAULT_POINTS, position);
	}
	
	public void hitByShell() {
		fall();
		level.addScoreWithNotification(DEFAULT_POINTS, position);
	}
	
	
	public boolean shouldBeRemoved() {
		return currentState == State.FALLING && position.y < -1 * getSize().y;
	}

	public boolean isDead() {
		return currentState == State.FALLING;
	}
}
