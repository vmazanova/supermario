package com.mygdx.mario.entities.enemies;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public class Goomba extends Enemy {
	public enum State {
		MOVING,
		SQUASHED,
		FALLING;
		
		TextureRegion texture;
		Animation animation;
		Sound sound;
		Rectangle head;
		Rectangle body;
	};
	
	private static int DEFAULT_POINTS = 100;
	
	private State currentState;
	private float timeToLive = 0.5f;

	public Goomba(Vector2 position, Level level) {
		super(Type.GOOMBA, position, new Vector2(-2.25f, 0), new Vector2(1, 1), level);
		
		this.canCollideWith.add(Entity.Type.KOOPA);
		this.canCollideWith.add(Entity.Type.GOOMBA);
		this.canCollideWith.add(Entity.Type.MARIO);
		
		currentState = State.MOVING;
	}

	public static void loadAssets(AssetManager assetManager) {
		Texture enemiesSheet = assetManager.get("enemies_sheet.png", Texture.class);
		
		TextureRegion[] movingAnimation = new TextureRegion[2];
		for (int j = 0; j < 2; j++) {
			movingAnimation[j] = new TextureRegion(enemiesSheet, j*16, 0*32+16, 16, 16);
		}
		
		State.MOVING.texture = movingAnimation[0];
		State.MOVING.animation = new Animation(0.15f, movingAnimation);
		State.SQUASHED.texture = new TextureRegion(enemiesSheet, 2*16, 0*32+16, 16, 16);
		State.FALLING.texture = new TextureRegion(enemiesSheet, 0*16, 0*32+16, 16, 16);
		State.FALLING.texture.flip(false, true);
		
		State.SQUASHED.sound = assetManager.get("sound/stomp.wav", Sound.class);
		State.FALLING.sound = assetManager.get("sound/kick.wav", Sound.class);
		
		State.MOVING.head = new Rectangle(0, 8f/16, 16f/16, 8f/16);
		State.MOVING.body = new Rectangle(0, 0, 16f/16, 8f/16);
	}
	
	public static TextureRegion getBaseTexture() {
		return State.MOVING.texture;
	}
	
	public Rectangle getHead() {
		return currentState.head;
	}
	
	public Rectangle getBody() {
		return currentState.body;
	}
	
	public void update(float deltaTime) {
		if (currentState == State.SQUASHED) {
			timeToLive -= deltaTime;
			return;
		}
		
		if (level.isOutOfLevel(position)) {
			currentState = State.FALLING;
		}
		
		velocity.y -= GRAVITY * deltaTime;
		
		if (!isDead()) {
			checkTileCollisions(this, deltaTime);
		}
		
		position.x += velocity.x * deltaTime;
		position.y += velocity.y * deltaTime;
	}

	public TextureRegion getTexture(float stateTime) {
		TextureRegion currentTexture;
		Animation stateAnimation = currentState.animation;
		if (stateAnimation != null) {
			currentTexture = stateAnimation.getKeyFrame(stateTime, true);
		} else {
			currentTexture = currentState.texture;
		}
		
		return currentTexture;
	}
	
	public void hit(Enemy enemy) {
		if (enemy.getPosition().x > position.x == velocity.x > 0) {
			velocity.x *= -1;
		}
	}
	
	public void stompedByMario(Mario mario) {
		currentState = State.SQUASHED;
		currentState.sound.play();
		level.addScoreWithNotification(DEFAULT_POINTS, position);

		mario.bounce();
	}
	
	public void kickedByMario(Mario mario) {
		mario.hitByEnemy();
	}
	
	public void fall() {
		currentState = State.FALLING;
		currentState.sound.play();
	}
	
	public void hitFromBellow() {
		fall();
		level.addScoreWithNotification(DEFAULT_POINTS, position);
	}
	
	public void hitByShell() {
		fall();
		level.addScoreWithNotification(DEFAULT_POINTS, position);
	}
	
	
	public boolean isDead() {
		return currentState == State.SQUASHED || currentState == State.FALLING;
	}
	
	public boolean shouldBeRemoved() {
		return ((currentState == State.SQUASHED && timeToLive <= 0) ||
			(currentState == State.FALLING && position.y <  -1 * getSize().y)
		);
	}
}
