package com.mygdx.mario.entities.enemies;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;

public abstract class Enemy extends Entity {
	protected boolean isSpawned = false;

	public Enemy(Type type, Vector2 position, Vector2 velocity, Vector2 size, Level level) {
		super(type, position, velocity, size, level);
	}
	
	public void spawn() {
		this.isSpawned = true;
	}
	
	public boolean isSpawned() {
		return isSpawned;
	}
	
	public void collideWithMovingTile(Vector2 tilePosition) {
		velocity.y = 8f;
		
		if (position.x < tilePosition.x) {
			if (velocity.x > 0) {
				velocity.x *= -1;
			}
		} else if (velocity.x < 0) {
			velocity.x *= -1;
		}
		
		hitFromBellow();
	}
	
	public abstract Rectangle getHead();
	public abstract Rectangle getBody();
	public abstract void hit(Enemy enemy);
	public abstract void stompedByMario(Mario mario);
	public abstract void kickedByMario(Mario mario);
	public abstract void hitFromBellow();
	public abstract void hitByShell();
	public abstract boolean shouldBeRemoved();
}
