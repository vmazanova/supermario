package com.mygdx.mario;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.mario.Tile.Type;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.entities.enemies.Enemy;
import com.mygdx.mario.entities.enemies.Goomba;
import com.mygdx.mario.entities.enemies.Koopa;
import com.mygdx.mario.entities.items.Coin;
import com.mygdx.mario.entities.items.Item;
import com.mygdx.mario.screens.EditorScreen;
import com.mygdx.mario.screens.EditorScreen.State;
import com.mygdx.mario.screens.editorWidgets.ThingPicker;
import com.mygdx.mario.screens.editorWidgets.ThingPicker.Things;
import com.badlogic.gdx.InputProcessor;

public class Canvas implements InputProcessor {
	private OrthographicCamera camera;
	private float cameraStep = 0.2f;
	private EditorScreen screen;
	
	private LevelData levelData;
	private Entity[][] entitiesArray;
	
	private ShapeRenderer shapeRenderer;
	private ThingPicker thingPicker;
	
	public Canvas(EditorScreen screen) {
		this.screen = screen;
		
		camera = screen.getCamera();
		thingPicker = screen.getTilePicker();
		shapeRenderer = new ShapeRenderer();
		
		this.levelData = LevelData.createEmptyLevel();
		entitiesArray = new Entity[levelData.height][levelData.width];
		
		camera.position.set(camera.viewportWidth/2.0f, levelData.height/2.0f, 0);
		screen.addInputProcessor(this);
	}
	
	private boolean isWithinViewport(float targetPositionX) {
		return targetPositionX > camera.position.x - camera.viewportWidth / 2 - 1 &&
				targetPositionX < camera.position.x + camera.viewportWidth / 2;
	}
	
	public void render (SpriteBatch spriteBatch, float stateTime) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			camera.translate(-cameraStep, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
			camera.translate(cameraStep, 0);
		}
		
		camera.update();
		
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(1, 1, 1, 1);
		for (int i = 0; i <= levelData.height; i++) {
			shapeRenderer.line(0, i, levelData.width, i);
		}
		for (int i = 0; i <= levelData.width; i++) {
			shapeRenderer.line(i, 0, i, levelData.height);
		}
		shapeRenderer.end();
		
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
		for (int j = 0; j < levelData.height; j++) {
			for (int i = 0; i < levelData.width; i++) {
				if (!isWithinViewport(i)) {
					continue;
				}
				
				TextureRegion tileTexture = levelData.tiles[j][i].getTexture(stateTime);
				if (tileTexture != null) {
					spriteBatch.draw(tileTexture, i, j, 1, 1);
				}
				
				if (entitiesArray[j][i] == null) {
					continue;
				}
				
				if (entitiesArray[j][i].getTexture(stateTime) != null) {
					spriteBatch.draw(entitiesArray[j][i].getTexture(stateTime), i, j, 1, entitiesArray[j][i].getSize().y);
				}
			}
		}
		
		spriteBatch.end();
	}

	public void dispose () {
		thingPicker.dispose();
		shapeRenderer.dispose();
	}
	
	private boolean isOutOfBounds(Vector3 coord) {
		if (coord.x < 0 || coord.x >= levelData.width || coord.y < 0 || coord.y >= levelData.height) {
			return true;
		}
		
		return false;
	}
	
	public String getLevelName() {
		return levelData.name;
	}
	
	public void setLevelName(String name) {
		levelData.name = name;
	}
	
	public LevelData getLevelData() {
		levelData.enemies.clear();
		levelData.items.clear();
		
		for (int j = 0; j < levelData.height; j++) {
			for (int i = 0; i < levelData.width; i++) {
				if (entitiesArray[j][i] != null) {
					if (entitiesArray[j][i] instanceof Enemy) {
						levelData.enemies.add((Enemy) entitiesArray[j][i]);
					} else if (entitiesArray[j][i] instanceof Item) {
						levelData.items.add((Item) entitiesArray[j][i]);
					} else if (entitiesArray[j][i] instanceof Mario) {
						levelData.marioPosition = entitiesArray[j][i].getPosition().cpy();
					}
				}
			}
		}
		
		if (levelData.marioPosition == null) {
			return null;
		}
		
		return levelData;
	}
	
	public void setLevelData(LevelData levelData) {
		this.levelData = levelData;
		
		entitiesArray = new Entity[levelData.height][levelData.width];
		
		Vector2 marioPosition = levelData.marioPosition;
		entitiesArray[(int) marioPosition.y][(int) marioPosition.x] = new Mario(marioPosition.cpy(), null);
		
		for (Entity entity : levelData.enemies) {
			Vector2 entPosition = entity.getPosition();
			entitiesArray[(int) entPosition.y][(int) entPosition.x] = entity;
		}
		for (Entity entity : levelData.items) {
			Vector2 entPosition = entity.getPosition();
			entitiesArray[(int) entPosition.y][(int) entPosition.x] = entity;
		}
		
		camera.position.set(camera.viewportWidth/2.0f, levelData.height/2.0f, 0);
	}
	
	public void changeLevelWidth(int newWidth) {
		if (newWidth != levelData.width) {
			Entity[][] newEntitiesArray = new Entity[entitiesArray.length][];
			for (int i = 0; i < entitiesArray.length; i++) {
				newEntitiesArray[i] = Arrays.copyOfRange(entitiesArray[i], 0, newWidth);
			}
			entitiesArray = newEntitiesArray;
			
			this.levelData = levelData.copyOfRange(newWidth, levelData.height);
		}
	}
	
	private void handleTileClick(int x, int y) {
		if (levelData.tiles[y][x].isTileEmpty()) {
			Tile newTile = new Tile(thingPicker.getSelectedTileType(), null, new Vector2(x, y));
			String selectedItem = thingPicker.getSelectedItem();
			generateItems(selectedItem, newTile);
			
			levelData.tiles[y][x] = newTile;
		} else {
			Tile selectedTile = levelData.tiles[y][x];
			selectedTile.printTileInfo();
		}
	}
	
	private void handleEntityClick(int x, int y) {
		if (entitiesArray[y][x] == null) {
			switch(thingPicker.getSelectedEntityType()) {
				case MARIO:
					entitiesArray[y][x] = new Mario(new Vector2(x, y), null);
					break;
				case KOOPA:
					entitiesArray[y][x] = new Koopa(new Vector2(x, y), null);
					break;
				case GOOMBA:
					entitiesArray[y][x] = new Goomba(new Vector2(x, y), null);
					break;
				case COIN:
					entitiesArray[y][x] = new Coin(new Vector2(x, y), null);
					break;
				default:
					break;
			}
		} else {
			System.out.print("This tile already has an entity: " + entitiesArray[y][x].getClass());
		}
	}
	
	private void generateItems(String selectedItem, Tile tile) {
		if (selectedItem == null) {
			return;
		}
		
		String[] parsedItem = selectedItem.toLowerCase().split("\\s+");
		int numberOfItems = 1;
		String itemName = "";
		
		if (parsedItem.length > 1) {
			numberOfItems = Integer.parseInt(parsedItem[0]);
			itemName = parsedItem[1];
		} else {
			itemName = parsedItem[0];
		}
		
		Item.Type itemType = null;
		for (final Item.Type i: Item.Type.values()) {
			if (itemName.contains(i.name().toLowerCase())) {
				itemType = i;
			}
		}
		
		if (itemType == null) {
			return;
		}
		
		for (int i = 0; i < numberOfItems; i++) {
			tile.isTreasure = true;
			tile.addItem(itemType);
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Vector3 coord = camera.unproject(new Vector3(screenX, screenY, 0));
		int x = (int) coord.x;
		int y = (int) coord.y;
		
        if (screen.getState() == State.EDITING && !isOutOfBounds(coord)) {
        	Things selectedThing = thingPicker.getSelectedThing();
        	switch(selectedThing) {
        		case TILES:
	        		if (button == Buttons.LEFT) {
	    	        	handleTileClick(x, y);
	            	} else if (button == Buttons.RIGHT) {
	            		if (!levelData.tiles[y][x].isTileEmpty()) {
	            			levelData.tiles[y][x] = new Tile(Type.EMPTY, null, new Vector2(x, y));
	            		}
	            	}
	        		break;
        		case ENTITIES:
	        		if (button == Buttons.LEFT) {
	    	        	handleEntityClick(x, y);
	            	} else if (button == Buttons.RIGHT) {
	            		if (entitiesArray[y][x] != null) {
	            			entitiesArray[y][x] = null;
	            		}
	            	}
	        		break;
        	}
        }
        return false;
    }

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
