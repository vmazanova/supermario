package com.mygdx.mario;

import java.util.LinkedList;
import java.util.Queue;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.entities.items.Item;

public class Tile {
	public enum Type {
		EMPTY(false, 0),
		STONE(true, 1),
		BRICK(true, 2),
		CUBE(true, 3),
		BOX(true, 4),
		EMPTY_BOX(true, 5),
		TREE_TRUNK(true, 6),
		TREE_CROWN_LEFT(true, 7),
		TREE_CROWN_MIDDLE(true, 8),
		TREE_CROWN_RIGHT(true, 9),
		VERTICAL_PIPE_LEFT_UPPER(true, 10),
		VERTICAL_PIPE_LEFT_LOWER(true, 11),
		VERTICAL_PIPE_RIGHT_UPPER(true, 12),
		VERTICAL_PIPE_RIGHT_LOWER(true, 13),
		HORIZONTAL_PIPE_LEFT_UPPER(true, 14),
		HORIZONTAL_PIPE_LEFT_LOWER(true, 15),
		HORIZONTAL_PIPE_MIDDLE_UPPER(true, 16),
		HORIZONTAL_PIPE_MIDDLE_LOWER(true, 17),
		HORIZONTAL_PIPE_RIGHT_UPPER(true, 18),
		HORIZONTAL_PIPE_RIGHT_LOWER(true, 19),
		SMILING_CLOUD(false, 20),
		CLOUD_LEFT_UPPER(false, 21),
		CLOUD_LEFT_LOWER(false, 22),
		CLOUD_MIDDLE_UPPER(false, 23),
		CLOUD_MIDDLE_LOWER(false, 24),
		CLOUD_RIGHT_UPPER(false, 25),
		CLOUD_RIGHT_LOWER(false, 26),
		CASTLE_TOWER(false, 27),
		CASTLE_TOWER_FILLED(false, 28),
		CASTLE_RIGHT_WINDOW(false, 29),
		CASTLE_LEFT_WINDOW(false, 30),
		CASTLE_WALL(false, 31),
		CASTLE_DOOR_UPPER(false, 32),
		CASTLE_DOOR_LOWER(false, 33),
		HILL_LEFT_SLOPE(false, 34),
		HILL_RIGHT_SLOPE(false, 35),
		HILL_TOP(false, 36),
		HILL_EMPTY(false, 37),
		HILL_LEFT_MARK(false, 38),
		HILL_RIGHT_MARK(false, 39),
		BUSH_LEFT(false, 40),
		BUSH_MIDDLE(false, 41),
		BUSH_RIGHT(false, 42),
		FLAG_POLE_TOP(false, 43),
		FLAG_POLE_BODY(false, 44);
		
		TextureRegion texture;
		Animation animation;
		boolean isBlocking;
		int id;
		
		Type (boolean isBlocking, int id) {
			this.isBlocking = isBlocking;
			this.id = id;
		}
		
		public TextureRegion getTexture() {
			return texture;
		}
	};
	
	public enum InteractionDirection {
		UP,
		DOWN,
		RIGHT,
		LEFT
	}
	
	protected boolean isRenderable = true;
	protected Type type;
	protected Vector2 position;
	protected static Sound bumpSound;
	
	private Level level;
	
	protected boolean isTreasure = false;
	protected Queue<Item.Type> items = new LinkedList<Item.Type>();
	
	public Tile(Type type, Level level, Vector2 position) {
		this.type = type;
		this.level = level;
		this.position = position;
	}
	
	public void setLevel(Level level) {
		this.level = level;
		
		if (type.equals(Type.FLAG_POLE_TOP)) {
			this.level.setFlagPole(this);
		}
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture tilesSheet = assetManager.get("tileset.png", Texture.class);
		
		TextureRegion[] boxAnimation = new TextureRegion[4];
		for (int j = 0; j < 3; j++) {
			boxAnimation[j] = new TextureRegion(tilesSheet, (24 + j) * 16, 0, 16, 16);
		}
		boxAnimation[3] = new TextureRegion(tilesSheet, 25 * 16, 0, 16, 16);
		
		Type.STONE.texture = new TextureRegion(tilesSheet, 0, 0, 16, 16);
		Type.BRICK.texture = new TextureRegion(tilesSheet, 1*16, 0, 16, 16);
		Type.CUBE.texture = new TextureRegion(tilesSheet, 0, 1*16, 16, 16);
		Type.BOX.texture = boxAnimation[0];
		Type.BOX.animation = new Animation(0.25f, boxAnimation);
		Type.EMPTY_BOX.texture = new TextureRegion(tilesSheet, 27*16, 0, 16, 16);
		Type.TREE_TRUNK.texture = new TextureRegion(tilesSheet, 5*16, 1*16, 16, 16);
		Type.TREE_CROWN_LEFT.texture = new TextureRegion(tilesSheet, 5*16, 8*16, 16, 16);
		Type.TREE_CROWN_MIDDLE.texture = new TextureRegion(tilesSheet, 6*16, 8*16, 16, 16);
		Type.TREE_CROWN_RIGHT.texture = new TextureRegion(tilesSheet, 7*16, 8*16, 16, 16);
		Type.VERTICAL_PIPE_LEFT_UPPER.texture = new TextureRegion(tilesSheet, 0, 8*16, 16, 16);
		Type.VERTICAL_PIPE_LEFT_LOWER.texture = new TextureRegion(tilesSheet, 0, 9*16, 16, 16);
		Type.VERTICAL_PIPE_RIGHT_UPPER.texture = new TextureRegion(tilesSheet, 1*16, 8*16, 16, 16);
		Type.VERTICAL_PIPE_RIGHT_LOWER.texture = new TextureRegion(tilesSheet, 1*16, 9*16, 16, 16);
		Type.HORIZONTAL_PIPE_LEFT_UPPER.texture = new TextureRegion(tilesSheet, 2*16, 8*16, 16, 16);
		Type.HORIZONTAL_PIPE_LEFT_LOWER.texture = new TextureRegion(tilesSheet, 2*16, 9*16, 16, 16);
		Type.HORIZONTAL_PIPE_MIDDLE_UPPER.texture = new TextureRegion(tilesSheet, 3*16, 8*16, 16, 16);
		Type.HORIZONTAL_PIPE_MIDDLE_LOWER.texture = new TextureRegion(tilesSheet, 3*16, 9*16, 16, 16);
		Type.HORIZONTAL_PIPE_RIGHT_UPPER.texture = new TextureRegion(tilesSheet, 4*16, 8*16, 16, 16);
		Type.HORIZONTAL_PIPE_RIGHT_LOWER.texture = new TextureRegion(tilesSheet, 4*16, 9*16, 16, 16);
		Type.SMILING_CLOUD.texture = new TextureRegion(tilesSheet, 4*16, 21*16, 16, 16);
		Type.CLOUD_LEFT_UPPER.texture = new TextureRegion(tilesSheet, 0, 20*16, 16, 16);
		Type.CLOUD_LEFT_LOWER.texture = new TextureRegion(tilesSheet, 0, 21*16, 16, 16);
		Type.CLOUD_MIDDLE_UPPER.texture = new TextureRegion(tilesSheet, 1*16, 20*16, 16, 16);
		Type.CLOUD_MIDDLE_LOWER.texture = new TextureRegion(tilesSheet, 1*16, 21*16, 16, 16);
		Type.CLOUD_RIGHT_UPPER.texture = new TextureRegion(tilesSheet, 2*16, 20*16, 16, 16);
		Type.CLOUD_RIGHT_LOWER.texture = new TextureRegion(tilesSheet, 2*16, 21*16, 16, 16);
		Type.CASTLE_TOWER.texture = new TextureRegion(tilesSheet, 11*16, 0, 16, 16);
		Type.CASTLE_TOWER_FILLED.texture = new TextureRegion(tilesSheet, 11*16, 1*16, 16, 16);
		Type.CASTLE_RIGHT_WINDOW.texture = new TextureRegion(tilesSheet, 12*16, 0, 16, 16);
		Type.CASTLE_LEFT_WINDOW.texture = new TextureRegion(tilesSheet, 14*16, 0, 16, 16);
		Type.CASTLE_WALL.texture = new TextureRegion(tilesSheet, 13*16, 0, 16, 16);
		Type.CASTLE_DOOR_UPPER.texture = new TextureRegion(tilesSheet, 12*16, 1*16, 16, 16);
		Type.CASTLE_DOOR_LOWER.texture = new TextureRegion(tilesSheet, 13*16, 1*16, 16, 16);
		Type.HILL_LEFT_SLOPE.texture = new TextureRegion(tilesSheet, 8*16, 8*16, 16, 16);
		Type.HILL_RIGHT_SLOPE.texture = new TextureRegion(tilesSheet, 10*16, 8*16, 16, 16);
		Type.HILL_TOP.texture = new TextureRegion(tilesSheet, 9*16, 8*16, 16, 16);
		Type.HILL_EMPTY.texture = new TextureRegion(tilesSheet, 9*16, 9*16, 16, 16);
		Type.HILL_LEFT_MARK.texture = new TextureRegion(tilesSheet, 10*16, 9*16, 16, 16);
		Type.HILL_RIGHT_MARK.texture = new TextureRegion(tilesSheet, 8*16, 9*16, 16, 16);
		Type.BUSH_LEFT.texture = new TextureRegion(tilesSheet, 11*16, 9*16, 16, 16);
		Type.BUSH_MIDDLE.texture = new TextureRegion(tilesSheet, 12*16, 9*16, 16, 16);
		Type.BUSH_RIGHT.texture = new TextureRegion(tilesSheet, 13*16, 9*16, 16, 16);
		Type.FLAG_POLE_TOP.texture = new TextureRegion(tilesSheet, 16*16, 8*16, 16, 16);
		Type.FLAG_POLE_BODY.texture = new TextureRegion(tilesSheet, 16*16, 9*16, 16, 16);
		
		bumpSound = assetManager.get("sound/bump.wav", Sound.class);
	}
	
	public Tile copy() {
		Tile newTile = new Tile(this.type, null, this.position);
		newTile.isTreasure = isTreasure;
		newTile.items = new LinkedList<Item.Type>(this.items);
		
		return newTile;
	}
	
	public Vector2 getPosition() {
		return position;
	}
	
	public Type getType() {
		return type;
	}
	
	public void restoreRenderable() {
		isRenderable = true;
	}
	
	public boolean isTileBlocking() {
		return type.isBlocking;
	}
	
	public boolean isTileEmpty() {
		return type == Type.EMPTY;
	}
	
	public TextureRegion getTexture(float stateTime) {
		Animation tileAnimation = type.animation;
		if (tileAnimation != null) {
			return tileAnimation.getKeyFrame(stateTime, true);
		}
		
		return type.texture;
	}
	
	public void interact(Entity entity, InteractionDirection dir) {
		if (!(entity instanceof Mario)) {
			return;
		}
		
		Mario mario = (Mario) entity;
		
		float offsetX = mario.getPosition().x;
		if (dir.equals(InteractionDirection.RIGHT)) {
			offsetX += 1f;
		}
		offsetX %= position.x;

		switch(type) {
			case FLAG_POLE_BODY:
				if (offsetX >= 0.42 && offsetX < 0.6) {
					level.marioTouchedFlag(this);
				}
				break;
			case CASTLE_DOOR_LOWER:
				level.castleReached();
				break;
			default:
				break;
		}
		
		if (!dir.equals(InteractionDirection.UP)) {
			return;
		}
		
		if (items.size() <= 0) {
			switch(type) {
				case EMPTY_BOX:
					bumpSound.play();
					break;
				case BRICK:
					if (mario.isBig()) {
						level.spawnSmashedBrick(this);
						type = Type.EMPTY;
					} else {
						bumpSound.play();
					}
					break;
				default:
					break;
			}
		} else {
			switch(items.remove()) {
				case COIN:
					level.spawnCoinEffect(position);
					break;
				case UP_MUSHROOM:
					level.spawnUpMushroom(position);
					break;
				case MAGIC_MUSHROOM:
					level.spawnMagicMushroom(position);
					break;
				default:
					break;
			}
		}
		
		if (type == Type.BOX || type == Type.BRICK) {
			if (isTreasure && items.size() <= 0) {
				type = Type.EMPTY_BOX;
			}
			
			isRenderable = false;
			level.spawnMovingTile(this);
		}
	}
	
	public void markAsTreasure() {
		isTreasure = true;
	}
	
	public void addItem(Item.Type item) {
		items.add(item);
	}
	
	public void printTileInfo() {
		System.out.print("TILE:: ");
		System.out.print("position: " + position.x + ", " + position.y + " -- ");
		System.out.print("type: " + type.name() + " -- ");
		if (items.size() > 0) {
			System.out.print("items: ");
			for (Item.Type item : items) {
				System.out.print(item.name() + ", ");
			}
		}
		
		System.out.println();
	}
}
