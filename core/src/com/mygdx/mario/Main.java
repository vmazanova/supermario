package com.mygdx.mario;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.mario.effects.Flag;
import com.mygdx.mario.effects.SmashedBrick;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.entities.enemies.Goomba;
import com.mygdx.mario.entities.enemies.Koopa;


public class Main extends ApplicationAdapter {
	private SpriteBatch spriteBatch;
	private OrthographicCamera camera;
	private AssetManager assetManager;
	
	private GameScreenManager gsm;
	
	@Override
	public void create () {
		camera = new OrthographicCamera();
		spriteBatch = new SpriteBatch();
		
		assetManager = new AssetManager();
		TextureParameter tpm = new TextureParameter();
		tpm.minFilter = TextureFilter.Nearest;
		tpm.magFilter = TextureFilter.Nearest;
		
		assetManager.load("mario_sheet.png", Texture.class);
		assetManager.load("enemies_sheet.png", Texture.class);
		assetManager.load("items_sheet.png", Texture.class);
		assetManager.load("tileset.png", Texture.class, tpm);
		assetManager.load("data/uiskin.json", Skin.class);
		assetManager.load("sound/main-theme.mp3", Music.class);
		assetManager.load("sound/jump-small.wav", Sound.class);
		assetManager.load("sound/jump-super.wav", Sound.class);
		assetManager.load("sound/coin.wav", Sound.class);
		assetManager.load("sound/bump.wav", Sound.class);
		assetManager.load("sound/mario-die.wav", Sound.class);
		assetManager.load("sound/stomp.wav", Sound.class);
		assetManager.load("sound/kick.wav", Sound.class);
		assetManager.load("sound/brick-smash.wav", Sound.class);
		assetManager.load("sound/stage-clear.wav", Sound.class);
		assetManager.load("sound/1-up.wav", Sound.class);
		assetManager.load("sound/powerup-appears.wav", Sound.class);
		assetManager.load("sound/powerup-pickup.wav", Sound.class);
		assetManager.load("sound/shrink.wav", Sound.class);
		assetManager.load("sound/flag-pole.wav", Sound.class);
		assetManager.load("sound/time-warning.wav", Sound.class);
		assetManager.load("sound/stage-clear.wav", Sound.class);
		assetManager.load("sound/game-over.mp3", Sound.class);
		assetManager.finishLoading();
		
		Tile.loadAssets(assetManager);
		
		// Entities
		Mario.loadAssets(assetManager);
		Goomba.loadAssets(assetManager);
		Koopa.loadAssets(assetManager);
		com.mygdx.mario.entities.items.Coin.loadAssets(assetManager);
		com.mygdx.mario.entities.items.Mushroom.loadAssets(assetManager);
		com.mygdx.mario.entities.items.MagicMushroom.loadAssets(assetManager);
		com.mygdx.mario.entities.items.UpMushroom.loadAssets(assetManager);
		
		// Effects
		com.mygdx.mario.effects.Coin.loadAssets(assetManager);
		Flag.loadAssets(assetManager);
		SmashedBrick.loadAssets(assetManager);
		
		gsm = new GameScreenManager(this);
	}

	@Override
	public void render () {
		gsm.render();
	}
	
	@Override
	public void dispose () {
		spriteBatch.dispose();
		assetManager.dispose();
	}
	
	@Override
	public void resize(int width, int height){
		camera.viewportWidth = width / 32f;
		camera.viewportHeight = height / 32f;
		camera.update();
		
		gsm.resize(width, height);
	}
	
	public OrthographicCamera getCamera() {
		return camera;
	}
	
	public SpriteBatch getSpriteBatch() {
		return spriteBatch;
	}
	
	public AssetManager getAssetManager() {
		return assetManager;
	}
}
