package com.mygdx.mario;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class Hud {
	private Stage stage;
	private World world;
	
	private Label scoreLabel;
	private Label coinsLabel;
	private Label worldLabel;
	private Label timeLabel;
	
	public Hud(World world) {
		stage = new Stage();
		this.world = world;
		
		Table table = new Table();
		table.top();
		table.setFillParent(true);
		table.pad(0, 200, 0, 200);
		
		scoreLabel = new Label(String.format("%06d", world.getScore()), new Label.LabelStyle(
				new BitmapFont(), Color.WHITE));
		coinsLabel = new Label(String.format("%02d", world.getCoins()), new Label.LabelStyle(
				new BitmapFont(), Color.WHITE));
		worldLabel = new Label(world.getLevelName(), new Label.LabelStyle(
				new BitmapFont(), Color.WHITE));
		timeLabel = new Label(String.format("%03d", world.getTimeLeft()), new Label.LabelStyle(
				new BitmapFont(), Color.WHITE));
		
		table.add(new Label("MARIO", new Label.LabelStyle(new BitmapFont(), Color.WHITE))).expandX().padTop(10);
		table.add(new Label("COINS", new Label.LabelStyle(new BitmapFont(), Color.WHITE))).expandX().padTop(10);
		table.add(new Label("WORLD", new Label.LabelStyle(new BitmapFont(), Color.WHITE))).expandX().padTop(10);
		table.add(new Label("TIME", new Label.LabelStyle(new BitmapFont(), Color.WHITE))).expandX().padTop(10);
		
		table.row();
		table.add(scoreLabel).expandX();
		table.add(coinsLabel).expandX();
		table.add(worldLabel).expandX();
		table.add(timeLabel).expandX();
		
		stage.addActor(table);
	}
	
	public void update(float deltaTime) {
		scoreLabel.setText(String.format("%06d", world.getScore()));
		coinsLabel.setText(String.format("%02d", world.getCoins()));
		worldLabel.setText(world.getLevelName());
		timeLabel.setText(String.format("%03d", world.getTimeLeft()));
	}
	
	public void render() {
		stage.draw();
	}
	
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}
	
	public void dispose() {
		stage.dispose();
	}
	
	public void setWorld(World world) {
		this.world = world;
	}
}
