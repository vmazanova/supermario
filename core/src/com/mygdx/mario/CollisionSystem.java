package com.mygdx.mario;

import java.util.List;

import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.entities.enemies.Enemy;
import com.mygdx.mario.entities.items.Item;

public class CollisionSystem {
	private Level level;
	
	public CollisionSystem(Level level) {
		this.level = level;
	}
	
	public void checkCollisions(List<Entity> entities, float deltaTime) {
		checkEntityCollisions(entities);
	}
	
	public void checkEntityCollisions(List<Entity> entities) {
		for (int i = 0; i < entities.size(); i++) {
			for (int j = i + 1; j < entities.size(); j++) {
				Entity entity1 = entities.get(i);
				Entity entity2 = entities.get(j);
				
				if (entity1.isDead() || entity2.isDead() || !entity1.canCollideWith(entity2)) {
					continue;
				}
				
				if (entity1 instanceof Enemy && !((Enemy) entity1).isSpawned() || 
						entity2 instanceof Enemy && !((Enemy) entity2).isSpawned()) {
					continue;
				}
				
				if (entity1.getPosition().x + entity1.size.x - 0.01f >= entity2.getPosition().x &&
						entity1.getPosition().x < entity2.getPosition().x + entity1.size.x - 0.01f &&
						entity1.getPosition().y <= entity2.getPosition().y + entity1.size.y - 0.01f &&
						entity1.getPosition().y + entity1.size.y - 0.01f > entity2.getPosition().y) {
					identifyContact(entity1, entity2);
				}
			}
		}
	}
	
	private void identifyContact(Entity ent1, Entity ent2) {
		if (ent1 instanceof Enemy && ent2 instanceof Enemy) {
			((Enemy) ent1).hit((Enemy) ent2);
			((Enemy) ent2).hit((Enemy) ent1);
			return;
		}
		
		if (!(ent1 instanceof Mario) && !(ent2 instanceof Mario)) {
			return;
		}
		
		Mario mario = null;
		Entity ent = null;
		if (ent2 instanceof Mario) {
			mario = (Mario) ent2;
			ent = ent1;
		} else {
			mario = (Mario) ent1;
			ent = ent2;
		}
		
		if (ent instanceof Enemy) {
			Enemy enemy = (Enemy) ent;
			if (mario.getPosition().y <= enemy.getPosition().y + enemy.getHead().y + enemy.getHead().height &&
					mario.getPosition().y > enemy.getPosition().y + enemy.getHead().y) {
				mario.stomp(enemy);
			} else if (mario.getPosition().y <= enemy.getPosition().y + enemy.getBody().y + enemy.getBody().height &&
					mario.getPosition().y + mario.size.y > enemy.getPosition().y) {
				mario.kick(enemy);
			}
		} else if (ent instanceof Item) {
			((Item) ent).use(mario);
		}
	}
}
