package com.mygdx.mario;

import com.mygdx.mario.screens.EditorScreen;
import com.mygdx.mario.screens.GameScreen;
import com.mygdx.mario.screens.MainMenuScreen;
import com.mygdx.mario.screens.PlayScreen;

public class GameScreenManager {
	private Main main;
	
	private GameScreen currentGameState;
	
	public enum Screen {
		MAIN_MENU,
		PLAY,
		EDITOR
	}
	
	public GameScreenManager(Main main) {
		this.main = main;
		setScreen(Screen.MAIN_MENU, null);
	}
	
	public void setScreen(Screen screen, LevelData levelData) {
		switch (screen) {
			case MAIN_MENU:
				currentGameState = new MainMenuScreen(this);
				break;
			case PLAY:
				currentGameState = new PlayScreen(this, levelData);
				break;
			case EDITOR:
				currentGameState = new EditorScreen(this);
				break;
			default:
				break;
		}
	}
	
	public void render() {
		currentGameState.render();
	}
	
	public void resize(int width, int height) {
		currentGameState.resize(width, height);
	}
	
	public Main getMain() {
		return main;
	}
}
