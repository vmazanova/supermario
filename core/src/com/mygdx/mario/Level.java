package com.mygdx.mario;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Tile.Type;
import com.mygdx.mario.effects.Coin;
import com.mygdx.mario.effects.Effect;
import com.mygdx.mario.effects.Flag;
import com.mygdx.mario.effects.MovingTile;
import com.mygdx.mario.effects.Notification;
import com.mygdx.mario.effects.SmashedBrick;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.entities.enemies.Enemy;
import com.mygdx.mario.entities.items.Item;
import com.mygdx.mario.entities.items.MagicMushroom;
import com.mygdx.mario.entities.items.UpMushroom;
import com.mygdx.mario.screens.PlayScreen;

public class Level {
	private OrthographicCamera camera;
	private float minCameraPositionX = 0;
	private World world;
	private CollisionSystem collisionSystem;
	
	private Mario mario;
	private LevelData levelData;
	private List<Effect> effects = new ArrayList<Effect>();
	private Flag flagPole;
	
	protected Music music;
	private Sound stageClear;
	private BitmapFont font;
	
	private float timeCount = 0;
	private int timeLeft = 300;
	
	private boolean holdingSpace = false;
	
	public enum State {
		IN_PROGRESS,
		WAITING_FOR_FLAG,
		GOING_TO_CASTLE,
		WON,
		LOST;
	};
	
	private State currentState;
	
	private float timeToLive = 5f;
	
	private ShapeRenderer shapeRenderer;
	
	public Level(PlayScreen screen, World world, LevelData levelData, Mario mario) {
		this.camera = screen.getCamera();
		this.stageClear = screen.getAssetManager().get("sound/stage-clear.wav", Sound.class);
		this.music = screen.getAssetManager().get("sound/main-theme.mp3", Music.class);
		this.music.setLooping(true);
		
		this.world = world;
		this.collisionSystem = new CollisionSystem(this);
		
		this.levelData = levelData;
		this.levelData.setLevel(this);
		
		this.mario = mario;
		this.shapeRenderer = new ShapeRenderer();
		
		camera.position.set(camera.viewportWidth / 2.0f, levelData.height / 2.0f, 0);
		camera.update();
		minCameraPositionX = camera.viewportWidth / 2.0f;
		
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		
		currentState = State.IN_PROGRESS;
	}
	
	public void handleInput() {
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			if (minCameraPositionX - camera.viewportWidth/2.0f + 0.25f <= mario.getPosition().x) {
				mario.moveLeft();
			}
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			mario.moveRight();
		}
		if (Gdx.input.isKeyPressed(Keys.SPACE)) {
			if (!holdingSpace) {
				mario.jump();
				holdingSpace = true;
			}
		} else {
			mario.finishJump();
			holdingSpace = false;
		}
	}
	
	private void updateTimeLeft(float deltaTime) {
		timeCount += deltaTime * 3;
		if (timeCount >= 1) {
			if (timeLeft > 0) {
				timeLeft--;
			} else {
				mario.die();
			}
			
			timeCount = 0;
		}
	}
	
	private void convertTimeToScore(float deltaTime) {
		timeCount += deltaTime * 50;
		if (timeCount >= 1) {
			if (timeLeft > 0) {
				timeLeft--;
				world.addScore(50);
			}
			
			timeCount = 0;
		}
	}
	
	public String getLevelName() {
		return levelData.name;
	}
	
	public int getTimeLeft() {
		return timeLeft;
	}
	
	public void update(float deltaTime) {
		switch(currentState) {
			case IN_PROGRESS:
				updateTimeLeft(deltaTime);
				handleInput();
				break;
			case GOING_TO_CASTLE:
				mario.moveRight();
				break;
			case WON:
				convertTimeToScore(deltaTime);
				break;
			default:
				break;
		}

		// resolveEntityCollisions();
		
		mario.update(deltaTime);
		
		if (currentState == State.LOST || currentState == State.WON) {
			if (timeToLive > 0) {
				timeToLive -= deltaTime;
			} else {
				if (currentState == State.LOST) {
					world.marioDied();
				} else if (timeLeft <= 0) {
					world.levelWon();
				}
			}
		}
		
		Iterator<Enemy> enemyIter = levelData.enemies.iterator();
		while (enemyIter.hasNext()) {
			Enemy enemy = enemyIter.next();   
			if (!enemy.isSpawned()) {
				if (isWithinViewport(enemy.getPosition().x)) {
					enemy.spawn();
				} else {
					continue;
				}
			}
			
			enemy.update(deltaTime);

			if (enemy.shouldBeRemoved()) {
				enemyIter.remove();
			}
		}
		
		Iterator<Item> itemIter = levelData.items.iterator();
		while (itemIter.hasNext()) {
			Item item = itemIter.next();   
			item.update(deltaTime);

			if (item.shouldBeRemoved()) {
				itemIter.remove();
			}
		}
		
		Iterator<Effect> effIter = effects.iterator();
		while (effIter.hasNext()) {
			Effect effect = effIter.next();    
			effect.update(deltaTime);
			
			if (!effect.isAlive()) {
				effIter.remove();
			}
		}
		
		List<Entity> entities = new ArrayList<Entity>();
		entities.add(mario);
		entities.addAll(levelData.enemies);
		entities.addAll(levelData.items);
		
		collisionSystem.checkEntityCollisions(entities);
	}
	
	private boolean isWithinViewport(float targetPositionX) {
		return targetPositionX > camera.position.x - camera.viewportWidth / 2.0f - 1 &&
				targetPositionX < camera.position.x + camera.viewportWidth / 2.0f;
	}
	
	private void drawItems(SpriteBatch spriteBatch, float stateTime, boolean drawBackgroundItems) {
		for (Item item: levelData.items) {
			Vector2 itemPosition = item.getPosition();
			if (!isWithinViewport(itemPosition.x) || (item.isAtBackground() != drawBackgroundItems)) {
				continue;
			}

			spriteBatch.draw(item.getTexture(stateTime), itemPosition.x, itemPosition.y, 1, 1);
		}
	}
	
	public void render(SpriteBatch spriteBatch, float stateTime) {
		Gdx.gl.glClearColor(107/255.0f, 140/255.0f, 1.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		Vector2 marioPosition = mario.getPosition();
		if (minCameraPositionX <= marioPosition.x) {
			camera.position.set(marioPosition.x, camera.position.y, 0);
			camera.update();
			
			minCameraPositionX = camera.position.x;
		}
		
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
		
		drawItems(spriteBatch, stateTime, true);
		
		for (int j = 0; j < levelData.height; j++) {
			for (int i = 0; i < levelData.width; i++) {
				if (isWithinViewport(i) && levelData.tiles[j][i].isRenderable) {
					TextureRegion texture = levelData.tiles[j][i].getTexture(stateTime);
					if (texture != null) {
						spriteBatch.draw(levelData.tiles[j][i].getTexture(stateTime), i, j, 1, 1);
					}
				}
			}
		}
		
		drawItems(spriteBatch, stateTime, false);

		for (Enemy enemy: levelData.enemies) {
			Vector2 enemyPosition = enemy.getPosition();
			if (!enemy.isSpawned() || !isWithinViewport(enemyPosition.x)) {
				continue;
			}
			
			spriteBatch.draw(enemy.getTexture(stateTime), enemyPosition.x, enemyPosition.y, 1, enemy.getSize().y);
		}
		
		for (Effect effect: effects) {
			effect.render(spriteBatch, stateTime);
		}
		
		if (currentState != State.WON) {
			mario.render(spriteBatch, stateTime);
		}
		
		spriteBatch.end();
		
		for (Enemy enemy: levelData.enemies) {
			Vector2 enemyPosition = enemy.getPosition();
			if (!enemy.isSpawned() || !isWithinViewport(enemyPosition.x)) {
				continue;
			}
			
			/*shapeRenderer.setProjectionMatrix(camera.combined);
			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(1, 0, 0, 1);
			shapeRenderer.rect(enemyPosition.x + enemy.getBody().x, enemyPosition.y + enemy.getBody().y, enemy.getBody().width, enemy.getBody().height);
			shapeRenderer.end();
			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(0, 0, 1, 1);
			shapeRenderer.rect(enemyPosition.x + enemy.getHead().x, enemyPosition.y + enemy.getHead().y, enemy.getHead().width, enemy.getHead().height);
			shapeRenderer.end();*/
		}
	}

	public boolean isOutOfLevel(Vector2 position) {
		if (position.x < 0 || position.y < 0) {
			return true;
		}
		
		return false;
	}
	
	public void marioDied() {
		currentState = State.LOST;
		music.stop();
	}
	
	public void notifyReward(Vector2 position, String text) {
		effects.add(new Notification(position, camera, font, text));
	}
	
	public void replaceEffectByNotification(Effect oldEffect, Vector2 position, String text) {
		int idx = effects.indexOf(oldEffect);
		effects.set(idx, new Notification(position, camera, font, text));
	}
	
	public void spawnCoinEffect(Vector2 position) {
		effects.add(new Coin(position, this));
	}
	
	public void spawnUpMushroom(Vector2 position) {
		levelData.items.add(new UpMushroom(position, this));
	}
	
	public void spawnMagicMushroom(Vector2 position) {
		levelData.items.add(new MagicMushroom(position, this));
	}
	
	public void addCoin() {
		world.addCoin();
	}
	
	public void addScoreWithNotification(int score, Vector2 position) {
		world.addScore(score);
		notifyReward(position, Integer.toString(score));
	}
	
	public void addScore(int score) {
		world.addScore(score);
	}
	
	public void addLife() {
		world.addLife();
	}
	
	public void resolveTileCollisions(Tile tile) {
		List<Entity> entities = new ArrayList<Entity>();
		entities.addAll(levelData.enemies);
		entities.addAll(levelData.items);
		
		for (Entity entity: entities) {
			Vector2 targetTile = new Vector2(tile.getPosition().x, tile.getPosition().y + 1f);
			
			if (entity.getPosition().x + 0.99f >= targetTile.x &&
					entity.getPosition().x < targetTile.x + 0.99f &&
					entity.getPosition().y <= targetTile.y + 0.99f &&
					entity.getPosition().y + entity.size.y - 0.01f > targetTile.y) {
				entity.collideWithMovingTile(targetTile);
			}
		}
	}
	
	public void spawnMovingTile(Tile tile) {
		effects.add(new MovingTile(tile));
		
		resolveTileCollisions(tile);
	}
	
	public void spawnSmashedBrick(Tile tile) {
		Vector2[] directions = {
			new Vector2(5f, 25f), 
			new Vector2(-5f, 25f),
			new Vector2(5f, 15f),
			new Vector2(-5f, 15f)
		};
		
		for (int i = 0; i < directions.length; i++) {
			effects.add(new SmashedBrick(tile.getPosition(), directions[i]));
		}
		
		this.addScore(50);
		
		resolveTileCollisions(tile);
	}
	
	public void setFlagPole(Tile tile) {
		flagPole = new Flag(tile.getPosition(), this);
		effects.add(flagPole);
	}
	
	public void marioTouchedFlag(Tile tile) {
		if (currentState == State.IN_PROGRESS) {
			music.stop();
			flagPole.activateEffect();
			mario.startClimbing();
			currentState = State.WAITING_FOR_FLAG;
		}
	}
	
	public void flagIsDown() {
		if (currentState == State.WAITING_FOR_FLAG) {
			currentState = State.GOING_TO_CASTLE;
		}
	}
	
	public void castleReached() {
		if (currentState == State.GOING_TO_CASTLE) {
			stageClear.play();
			currentState = State.WON;
		}
	}
	
	private Vector2 fitToTile(float x, float y) {
		return new Vector2((int) x, (int) Math.ceil(y));
	}
	
	private boolean isOutOfBounds(Vector2 tileCoords) {
		if (tileCoords.x < 0 || tileCoords.x >= levelData.width || tileCoords.y < 0 || tileCoords.y >= levelData.height) {
			return true;
		}
		
		return false;
	}
	
	public Tile getTile(float x, float y) {
		Vector2 tileCoords = fitToTile(x, y);
		
		if (isOutOfBounds(tileCoords)) {
			return new Tile(Type.EMPTY, this, tileCoords);
		}
		
		return levelData.tiles[(int) tileCoords.y][(int) tileCoords.x];
	}

	public Vector2 getTileCoords(float x, float y) {
		Vector2 tileCoords = fitToTile(x, y);
		
		if (isOutOfBounds(tileCoords)) {
			return null;
		}
		
		return tileCoords;
	}
	
	public void dispose() {
		font.dispose();
	}
}
