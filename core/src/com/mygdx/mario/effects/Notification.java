package com.mygdx.mario.effects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

public class Notification extends Effect {
	private OrthographicCamera camera;
	private BitmapFont font;
	
	private float offset;
	private String text;
	
	private float timeToLive = 0.6f;

	public Notification(Vector2 position, OrthographicCamera camera, BitmapFont font, String text) {
		super(new Vector2(position.x, position.y + 1f), new Vector2(0, 5f));
        
		this.camera = camera;
		this.font = font;
		this.text = text;
		
		this.offset = camera.position.x - position.x;
		
		isActive = true;
	}

	public void update(float time) {
		position.x = camera.position.x - offset;
		position.y += velocity.y * time;
		
		timeToLive -= time;
	}
	
	private Vector2 getUnitsInPixel() {
		float x = camera.viewportWidth / Gdx.graphics.getWidth();
		float y = camera.viewportHeight / Gdx.graphics.getHeight();
		
		return new Vector2(x, y);
	}
	
	private Vector2 getPixelsInUnit() {
		float x = Gdx.graphics.getWidth() / camera.viewportWidth;
		float y = Gdx.graphics.getHeight() / camera.viewportHeight;
		
		return new Vector2(x, y);
	}

	public void render(SpriteBatch spriteBatch, float time) {
		Matrix4 originalMatrix = spriteBatch.getProjectionMatrix().cpy();
		
		Vector2 unitsInPixel = getUnitsInPixel();
		spriteBatch.setProjectionMatrix(originalMatrix.cpy().scale(unitsInPixel.x, unitsInPixel.y, 1));
		
		Vector2 pixelsInUnit = getPixelsInUnit();
		font.draw(spriteBatch, text, position.x * pixelsInUnit.x, position.y * pixelsInUnit.y);
		
		spriteBatch.setProjectionMatrix(originalMatrix);
	}
	
	public boolean isAlive() {
		return timeToLive > 0;
	}
}
