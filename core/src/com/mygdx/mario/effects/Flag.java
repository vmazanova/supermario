package com.mygdx.mario.effects;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;

public class Flag extends Effect {
	public static TextureRegion classTexture;
	public static Sound sound;
	private Level level;
	
	private int POINTS = 1000;
	
	public Flag(Vector2 position, Level level) {
		super(new Vector2(position.x - 0.5f, position.y - 1f), new Vector2(0, 0), classTexture);
		
		this.level = level;
		gravity = 10f;
		isActive = false;
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture tilesSheet = assetManager.get("items_sheet.png", Texture.class);
		
		classTexture = new TextureRegion(tilesSheet, 8 * 16, 2 * 16, 16, 16);
		sound = assetManager.get("sound/flag-pole.wav", Sound.class);
	}
	
	public void activateEffect() {
		isActive = true;
		sound.play();
		level.addScoreWithNotification(POINTS, new Vector2(position.x + 1f, position.y - 3f));
	}
	
	@Override
	public void update(float deltaTime) {
		if (!isActive) {
			return;
		}
		
		velocity.y -= gravity * deltaTime;
		
		Vector2 nextPosition = new Vector2(position.x + velocity.x * deltaTime, position.y + velocity.y * deltaTime);

		// solid tile below flag
		if (velocity.y < 0 &&
				(level.getTile(position.x + 0.99f, nextPosition.y - 0.99f).isTileBlocking() ||
				level.getTile(position.x, nextPosition.y - 0.99f).isTileBlocking())) {
			
			// get flag position with resolved collision
			float newPosY = level.getTileCoords(position.x, nextPosition.y).y;
			if (newPosY <= position.y) {
				position.y = newPosY;
			}
			
			velocity.y = 0;
			
			this.level.flagIsDown();
			isActive = false;
		}
	    
	    position.y += velocity.y * deltaTime;
	}
}