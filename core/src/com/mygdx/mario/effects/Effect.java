package com.mygdx.mario.effects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public abstract class Effect {
	protected Vector2 position, velocity;
	protected float gravity = 70f;
	
	protected boolean isActive = false;
	
	protected TextureRegion texture;
	protected Animation animation;
	
	public Effect(Vector2 position, Vector2 velocity, Animation animation) {
		this(position, velocity);
		this.animation = animation;
	}
	
	public Effect(Vector2 position, Vector2 velocity, TextureRegion texture) {
		this(position, velocity);
		this.texture = texture;
	}
	
	public Effect(Vector2 position, Vector2 velocity) {
		this.position = position;
		this.velocity = velocity;
	}
	
	public Vector2 getPosition() {
		return position;
	}
	
	public TextureRegion getTexture(float stateTime) {
		if (animation != null) {
			return animation.getKeyFrame(stateTime, true);
		}
		
		return texture;
	}
	
	public void render(SpriteBatch spriteBatch, float stateTime) {
		spriteBatch.draw(getTexture(stateTime), position.x, position.y, 1, 1);
	}
	
	public boolean isAlive() {
		return true;
	}
	
	public abstract void update(float time);
}
