package com.mygdx.mario.effects;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class SmashedBrick extends Effect {
	public static Animation animation;
	public static Sound sound;

	public SmashedBrick(Vector2 position, Vector2 velocity) {
		super(position.cpy(), velocity.cpy(), animation);

		isActive = true;
		sound.play();
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture tilesSheet = assetManager.get("items_sheet.png", Texture.class);
		
		TextureRegion[] brickPieceAnimation = new TextureRegion[2];
		for (int j = 0; j < 2; j++) {
			brickPieceAnimation[j] = new TextureRegion(tilesSheet, 4 * 16, (j+1) * 16, 16, 16);
		}
		
		animation = new Animation(0.35f, brickPieceAnimation);
		sound = assetManager.get("sound/brick-smash.wav", Sound.class);
	}

	@Override
	public void update(float time) {
		velocity.y -= gravity * time;
		position.y += velocity.y * time;
		position.x += velocity.x * time;
	}

	public boolean isAlive() {
		return position.y > 0;
	}
}
