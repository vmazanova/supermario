package com.mygdx.mario.effects;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Tile;

public class MovingTile extends Effect {
	private Tile tile;
	private float startingPositionY;

	public MovingTile(Tile tile) {
		super(tile.getPosition().cpy(), new Vector2(0, 5f), tile.getType().getTexture());
		
		this.tile = tile;
		gravity = 25f;
		startingPositionY = tile.getPosition().y;
		isActive = true;
	}
	
	public void update(float time) {
		velocity.y -= gravity * time;
		position.y += velocity.y * time;
	}
	
	public boolean isAlive() {
		if (velocity.y > 0 || position.y > startingPositionY) {
			return true;
		}
		
		tile.restoreRenderable();
		return false;
	}
}
