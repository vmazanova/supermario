package com.mygdx.mario.effects;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.Level;

public class Coin extends Effect {
	private Level level;
	
	public static Animation animation;
	public static Sound sound;
	
	private float startingPositionY;
	
	private int POINTS = 200;
	
	public Coin(Vector2 position, Level level) {
		super(new Vector2(position.x, position.y + 1f), new Vector2(0, 23f), animation);
		
		this.level = level;
		
		startingPositionY = position.y + 1f;
		isActive = true;
		sound.play();
		level.addCoin();
	}
	
	public static void loadAssets(AssetManager assetManager) {
		Texture itemsSheet = assetManager.get("items_sheet.png", Texture.class);
		
		TextureRegion[] coinAnimation = new TextureRegion[4];
		for (int j = 0; j < 4; j++) {
			coinAnimation[j] = new TextureRegion(itemsSheet, j*16, 7*16, 16, 16);
		}
		
		animation = new Animation(0.15f, coinAnimation);
		sound = assetManager.get("sound/coin.wav", Sound.class);
	}
	
	public void update(float time) {
		velocity.y -= gravity * time;
		position.y += velocity.y * time;
		
		if (velocity.y <= 0 && position.y < startingPositionY) {
			level.replaceEffectByNotification(this, position, Integer.toString(POINTS));
		}
	}
}
