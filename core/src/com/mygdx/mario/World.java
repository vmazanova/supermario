package com.mygdx.mario;

import java.io.IOException;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.mario.entities.Mario;
import com.mygdx.mario.screens.PlayScreen;

public class World {
	private Level level;
	private static LevelData levelData;
	private PlayScreen screen;
	
	private Sound lifeAdded;
	private Sound coinAdded;
	
	private Mario mario;
	
	private int score = 0;
	private int coins = 0;
	private int lives = 3;
	
	public World(PlayScreen screen) {
		this.screen = screen;
		lifeAdded = screen.getAssetManager().get("sound/1-up.wav", Sound.class);
		coinAdded = screen.getAssetManager().get("sound/coin.wav", Sound.class);
		
		if (levelData == null) {
			try {
				levelData = LevelData.loadFirstLevel();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (levelData == null) {
			screen.worldFinished();
		}
		
		mario = new Mario(levelData.marioPosition.cpy(), null);
		level = new Level(screen, this, levelData.copy(), mario);
		mario.setLevel(level);
	}
	
	public static void setLevelData(LevelData levelDataArg) {
		levelData = levelDataArg;
	}
	
	public void update(float deltaTime) {	
		level.update(deltaTime);
	}
	
	public void render(SpriteBatch spriteBatch, float stateTime) {
		level.render(spriteBatch, stateTime);
	}
	
	public void addLife() {
		lives++;
		lifeAdded.play();
	}
	
	public void addCoin() {
		coins++;
		score += 200;
		coinAdded.play();
		
		if (coins >= 100) {
			addLife();
			coins = 0;
		}
	}
	
	public void addScore(int score) {
		this.score += score;
	}
	
	public String getLevelName() {
		String levelName = level.getLevelName();
		levelName = levelName.replace("world_", "");
		
		return levelName;
	}
	
	public int getCoins() {
		return coins;
	}
	
	public int getScore() {
		return score;
	}
	
	public int getLives() {
		return lives;
	}
	
	public int getTimeLeft() {
		return level.getTimeLeft();
	}
	
	public void marioDied() {
		lives--;
		if (lives > 0) {
			mario = new Mario(levelData.marioPosition.cpy(), null);
			level = new Level(screen, this, levelData.copy(), mario);
			mario.setLevel(level);
			screen.levelFinished();
		} else {
			screen.gameOver();
		}
	}
	
	public void levelWon() {
		try {
			levelData = levelData.loadNextLevel();
			if (levelData != null) {
				mario.setPosition(levelData.marioPosition.cpy());
				level = new Level(screen, this, levelData.copy(), mario);
				mario.setLevel(level);
				screen.levelFinished();
			} else {
				screen.worldFinished();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stopMusic() {
		level.music.stop();
	}
	
	public void playMusic() {
		level.music.play();
	}
	
	public void pauseMusic() {
		level.music.pause();
	}
	
	public void dispose() {
		level.dispose();
	}
}
