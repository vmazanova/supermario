package com.mygdx.mario;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.mario.entities.Entity;
import com.mygdx.mario.entities.enemies.Enemy;
import com.mygdx.mario.entities.enemies.Goomba;
import com.mygdx.mario.entities.enemies.Koopa;
import com.mygdx.mario.entities.items.Coin;
import com.mygdx.mario.entities.items.Item;

public class LevelData {
	private static String LEVELS_PATH = "levels/";
	
	Map<Integer, Tile.Type> tileIdMap;
	Map<Integer, Item.Type> itemIdMap;
	Map<Integer, Entity.Type> entityIdMap;

	protected Level level;
	
	protected Tile[][] tiles;
	protected Vector2 marioPosition;
	protected List<Enemy> enemies = new ArrayList<Enemy>();
	protected List<Item> items = new ArrayList<Item>();
	
	protected String name;
	protected int width;
	protected int height;
	
	private LevelData() {
		tileIdMap = new HashMap<Integer, Tile.Type>();
		for (final Tile.Type t: Tile.Type.values()) {
			tileIdMap.put(t.id, t);
		}
		
		itemIdMap = new HashMap<Integer, Item.Type>();
		for (final Item.Type i: Item.Type.values()) {
			itemIdMap.put(i.id, i);
		}
		
		entityIdMap = new HashMap<Integer, Entity.Type>();
		for (final Entity.Type i: Entity.Type.values()) {
			entityIdMap.put(i.id, i);
		}
	}
	
	public LevelData copy() {
		return this.copyOfRange(this.width, this.height);
	}
	
	public LevelData copyOfRange(int newWidth, int newHeight) {
		LevelData levelData = new LevelData();
		levelData.name = this.name;
		
		levelData.width = newWidth;
		levelData.height = newHeight;
		
		levelData.tiles = new Tile[newHeight][newWidth];
		for (int j = 0; j < newHeight; j++) {
			for (int i = 0; i < newWidth; i++) {
				if (this.width > i && this.height > j) {
					levelData.tiles[j][i] = this.tiles[j][i].copy();
				} else {
					Vector2 tilePosition = new Vector2(i, this.height - j - 1);
					levelData.tiles[this.height - j - 1][i] = new Tile(Tile.Type.EMPTY, null, tilePosition);
				}
			}
		}
		
		levelData.marioPosition = this.marioPosition.cpy();
		
		for (Enemy en: this.enemies) {
			Vector2 enemyPos = en.getPosition();
			if (enemyPos.x < newWidth && enemyPos.y < newHeight) {
				switch(en.getType()) {
					case KOOPA:
						levelData.enemies.add(new Koopa(new Vector2(enemyPos.x, enemyPos.y), null));
						break;
					case GOOMBA:
						levelData.enemies.add(new Goomba(new Vector2(enemyPos.x, enemyPos.y), null));
						break;
					default:
						break;
				}
			}
		}
		
		for (Item item: this.items) {
			Vector2 itemPos = item.getPosition();
			if (itemPos.x < newWidth && itemPos.y < newHeight) {
				switch(item.getType()) {
					case COIN:
						levelData.items.add(new Coin(new Vector2(itemPos.x, itemPos.y), null));
						break;
					default:
						break;
				}
			}
		}
		
		return levelData;
	}
	
	public Tile getTile(Vector2 tileCoords) {
		return tiles[(int) tileCoords.y][(int) tileCoords.x];
	}
	
	public void setLevel(Level level) {
		this.level = level;
		
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				tiles[j][i].setLevel(level);
			}
		}
		
		for (Enemy enemy: enemies) {
			enemy.setLevel(level);
		}
		
		for (Item item: items) {
			item.setLevel(level);
		}
	}
	
	public static String[] listLevelFiles() {
		return listLevelFiles("");
	}
	
	public static String[] listLevelFiles(String prefix) {
		File folder = new File(LEVELS_PATH);
		File[] filesList = folder.listFiles();
		
		List<String> fileNamesList = new ArrayList<String>();
		for (int i = 0; i < filesList.length; i++) {
			String fileName = filesList[i].getName();
			if (fileName.toLowerCase().startsWith(prefix) && fileName.toLowerCase().endsWith(".lvl")) {
				int lastPeriodPos = fileName.lastIndexOf('.');
				if (lastPeriodPos > 0) {
					fileName = fileName.substring(0, lastPeriodPos);
					fileNamesList.add(fileName);
				}
			}
		}

		String[] fileNamesArray = new String[fileNamesList.size()];
		fileNamesArray = fileNamesList.toArray(fileNamesArray);
		
		return fileNamesArray;
	}
	
	public static LevelData createEmptyLevel() {
		LevelData levelData = new LevelData();
		levelData.name = "test";
		
		levelData.height = 13;
		levelData.width = 110;
		
		levelData.tiles = new Tile[levelData.height][levelData.width];
		
		for (int j = 0; j < levelData.height; j++) {
			for (int i = 0; i < levelData.width; i++) {
				Vector2 tilePosition = new Vector2(i, levelData.height - j - 1);
				levelData.tiles[levelData.height - j - 1][i] = new Tile(Tile.Type.EMPTY, null, tilePosition);
			}
		}
		
		return levelData;
	}
	
	public void saveLevel(String fileName) throws IOException {
		DataOutputStream os = new DataOutputStream(new FileOutputStream(LEVELS_PATH + fileName + ".lvl"));
		
		os.writeBytes("lvl");
		os.writeByte(1);
		os.writeByte(height);
		os.writeByte(width);
		
		os.writeFloat(marioPosition.x);
		os.writeFloat(marioPosition.y);
		
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				os.writeByte(tiles[j][i].type.id);
				os.writeByte(tiles[j][i].items.size());
				for (Item.Type item : tiles[j][i].items) {
					os.writeByte(item.id);
				}
			}
		}
		
		os.writeByte(enemies.size() + items.size());
		for (Enemy enemy : enemies) {
			os.writeByte(enemy.getType().id);
			Vector2 enemyPosition = enemy.getPosition();
			os.writeFloat(enemyPosition.x);
			os.writeFloat(enemyPosition.y);
		}
		for (Item item : items) {
			os.writeByte(item.getType().id);
			Vector2 itemPosition = item.getPosition();
			os.writeFloat(itemPosition.x);
			os.writeFloat(itemPosition.y);
		}
		
		os.close();
	}
	
	public LevelData loadNextLevel() throws IOException {
		String[] fileNames = listLevelFiles("world");
		Arrays.sort(fileNames);
		
		String nextLevelName = "";
		for (int i = 0; i < fileNames.length; i++) {
			if (fileNames[i].equals(name)) {
				if ((i + 1) < fileNames.length) {
					nextLevelName = fileNames[i+1];
				}
				break;
			}
		}
		
		if (nextLevelName != "") {
			return loadLevel(nextLevelName);
		}
		
		return null;
	}
	
	public static LevelData loadFirstLevel() throws IOException {
		String[] fileNames = listLevelFiles("world");
		Arrays.sort(fileNames);

		if (fileNames.length > 0) {
			return loadLevel(fileNames[0]);
		}
		
		return null;
	}
	
	public static LevelData loadLevel(String fileName) throws IOException {
		LevelData levelData = new LevelData();
		levelData.name = fileName;
		
		DataInputStream is = new DataInputStream(new FileInputStream(LEVELS_PATH + fileName + ".lvl"));
		
		byte[] signature = new byte[3];
		is.read(signature, 0, 3);
		int fileVersion = is.readByte() & 0xFF;
		
		levelData.height = is.readByte() & 0xFF;
		levelData.width = is.readByte() & 0xFF;

		levelData.tiles = new Tile[levelData.height][levelData.width];
		
		levelData.marioPosition = new Vector2(is.readFloat(), is.readFloat());
		
		for (int j = 0; j < levelData.height; j++) {
			for (int i = 0; i < levelData.width; i++) {
				int tileId = is.readByte() & 0xFF;
				Vector2 tilePosition = new Vector2(i, j);
				
				Tile newTile = new Tile(levelData.tileIdMap.get(tileId), null, tilePosition);
				
				int numberOfItems = is.readByte() & 0xFF;
				for (int k = 0; k < numberOfItems; k++) {
					int itemId = is.readByte() & 0xFF;
					newTile.markAsTreasure();
					newTile.addItem(levelData.itemIdMap.get(itemId));
				}
				
				levelData.tiles[j][i] = newTile;
			}
		}
		
		int numberOfEntities = is.readByte() & 0xFF;
		for (int i = 0; i < numberOfEntities; i++) {
			int entityId = is.readByte() & 0xFF;
			switch(levelData.entityIdMap.get(entityId)) {
				case KOOPA:
					levelData.enemies.add(new Koopa(new Vector2(is.readFloat(), is.readFloat()), null));
					break;
				case GOOMBA:
					levelData.enemies.add(new Goomba(new Vector2(is.readFloat(), is.readFloat()), null));
					break;
				case COIN:
					levelData.items.add(new Coin(new Vector2(is.readFloat(), is.readFloat()), null));
					break;
				default:
					break;
			}
		}

		is.close();
		
		return levelData;
	}
}
